<?php 
	error_reporting(0);
	session_start();
	require_once 'Users.php';
	
	/**
	  * Filter to check what kind of request is coming in
	  * and appropriately call the required function
	  *
	  **/
	if(isset($_POST['authenticate'])){
		$remoteIp = $_SERVER['REMOTE_ADDR'];	
		if(!isset($_SESSION["$remoteIp"])){
			$_SESSION["$remoteIp"] = 1;
		}
		else{
			if($_SESSION["$remoteIp"] > 3){
				$_SESSION['error'] = "You have exceeeded the maximum number of tries ! Please try again after sometime.";
				header("Location: http://localhost:80/miniproject/loginView.php");
				return true;
			}
			else{
				$_SESSION["$remoteIp"] += 1; 
				authenticate();
			}
		}
	}
	else if(isset($_POST['register'])){
		register();
	}
	
	/**
	  * Function to authenticate user using new Users object
	  * Parameters are recieved as a POST CALL
	  * Post Params : 'username' & 'password'
	  **/
	function authenticate(){
		$username = $_POST['username'];
		$password = $_POST['password'];
		if(isset($username) && isset($password)){
			$user = new Users();
			$isUserAuthentic = $user->authenticateUser($username,$password);
			if($isUserAuthentic == false){
				$_SESSION['error'] = "Invalid user name and password";
				header("Location: http://localhost:80/miniproject/loginView.php");
			}
			else{
				unset($_SESSION["$remoteIp"]);
				header("Location: http://localhost:80/miniproject/dashboardView.php");
			}
		}
	}
	
	/**
	  * Function to register user using new Users object
	  * Parameters are recieved in a POST CALL
	  * Post Params : 'username' , 'password', 'name' & 'email'
	  **/
	function register(){
		$data['username'] = $_POST['username'];
		$data['password'] = $_POST['password'];
		$data['name'] = $_POST['name'];
		$data['email'] = $_POST['email'];
		if(isset($data['username']) && $data['username'] != "" && isset($data['password'])&& $data['password'] != "" && isset($data['name']) && $data['name'] != "" && isset($data['email']) && $data['email'] != ""){
			$user = new Users();
			$isUserRegistered = $user->registerUser($data);
			if($isUserRegistered == false){
				$_SESSION['error'] = "Error while registering user ! Please try again";
				header("Location: http://localhost:80/miniproject/registerView.php");
				return false;
			}
			else{
				header("Location: http://localhost:80/miniproject/dashboardView.php");
				return true;
			}
		}	
		
		//Code to check and display error message on view page.
		$errorString;
		if(!isset($data['username']) || $data['username']== ""){
				$errorString.= "Username is invalid. <br/>";
		}
		if(!isset($data['password']) || $data['password']== ""){
				$errorString.= "Password is invalid. <br/>";
		}
		if(!isset($data['email']) || $data['email']== ""){
				$errorString.= "Email is invalid. <br/>";
		}
		if(!isset($data['name']) || $data['name']== ""){
				$errorString.= "Full Name is invalid. <br/>";
		}
		$_SESSION['error'] = $errorString;
		header("Location: http://localhost:80/miniproject/registerView.php");
	}
?>