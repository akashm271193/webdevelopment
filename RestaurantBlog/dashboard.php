<?php 
	error_reporting(0);
	require_once 'Users.php';
	require_once 'Restaurants.php';

/**
  * Check if UserId is set in session variable.
  * IF not, then either session has expired, or someone is trying direct access
  * and throws the user back to login page.
  * Returns: null 
  **/
if(!isset($_SESSION['userId'])){
	header("Location: http://localhost:80/miniproject/loginView.php");
}

/**
  * Check for the request method coming in
  * and based on post variable set, calls the appropriate method
  * Returns: null
  **/
if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['addRestaurant'])){
	addRestaurant();
}
else if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['addReview'])){
	addReview();
}
else if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['logout'])){
	logout();
}

/**
  * Function to add restaurant
  * if userId is not present in session, sends user back to home page
  * if successful takes the user to dashboard page.
  * Parameters are recieved as a POST CALL
  * Post Params : 'name' , 'location'  & 'review'
  **/
function addRestaurant(){
	if(isset($_SESSION['userId'])){
		$data['userId'] = $_SESSION['userId'];
		$data['name'] = $_POST['name'];
		$data['location'] = $_POST['location'];
		if($data['name'] == null || trim($data['name']) == "" ||$data['location'] == null || trim($data['location'] == "")){
			$_SESSION['error'] = "Invalid entry! Please check your details once again.";
			header("Location: http://localhost:80/miniproject/addRestaurant.php");
			return false;
		}
		if(isset($_POST['review']) && $_POST['review'] != ""){
			$data['review'] = addslashes($_POST['review']);
		}
		$restaurants = new Restaurants();
		$result = $restaurants->addRestaurant($data);
		if($result === -1){
			$_SESSION['error'] = "Restaurant Already Exists ! You can add review for the same.";
		}
		else if($result === true){
			$_SESSION['message'] = "Successfully added restaurant !";
			header("Location: http://localhost:80/miniproject/dashboardView.php");
			return true;
		}
		else{
			$_SESSION['error'] = "Error while adding restaurant! Please try again after sometime.";
		}
		header("Location: http://localhost:80/miniproject/dashboardView.php");
		return true;
	}
	else{
		header("Location: http://localhost:80/miniproject/loginView.php");
	}
}


/**
  * Function to add review
  * if userId is not present in session, sends user back to home page
  * if successful takes the user to dashboard page.
  * Parameters are recieved as a POST CALL
  * Post Params : 'userId' , 'restaurantId'  & 'review'  
  **/
function addReview(){
	if(isset($_SESSION['userId'])){
		$data['userId'] = $_SESSION['userId'];
		$data['restaurantId'] = $_POST['restaurantId'];
		$data['restaurantName'] = $_POST['restaurantName'];
		$error = "";
		if(!isset($_POST['review']) || $_POST['review'] == ""){
			$error = "Please enter a review to add !";		
		}
		else{
			$data['review'] = addslashes($_POST['review']);
			$restaurants = new Restaurants();
			$result = $restaurants->addReview($data);
			if($result == true){
				$_SESSION['message'] = "Successfully added your review !";
				header("Location: http://localhost:80/miniproject/dashboardView.php");
				return true;
			}
			else{
				$error = "Error while adding review ! Please try again after sometime";	
			}
		}
		$ch = curl_init("http://localhost:80/miniproject/addReview.php");
		$encoded = urlencode("restaurantId").'='.urlencode($data['restaurantId']).'&'.urlencode("restaurantName").'='.urlencode($data['restaurantName']);
		if($error != ""){
			$encoded .='&'.urlencode("error").'='.urlencode($error);
		}
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,  $encoded);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_exec($ch);
	}
	else{
		$_SESSION['error'] = "Invalid Session!";
		header("Location: http://localhost:80/miniproject/loginView.php");
	}
}

/**
  * Function to log user out
  * Destroys the entire user session and sends the user back to the homepage.
  **/
function logout(){
	if(isset($_SESSION['userId'])){
		session_destroy();
		header("Location: http://localhost:80/miniproject/loginView.php");
	}
}
?>