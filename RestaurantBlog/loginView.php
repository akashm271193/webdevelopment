<html>
	<head>
		<title>Login</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<div id="main">
		<?php 
				require_once 'login.php';
				//var_dump($_SESSION);
				if(isset($_SESSION["error"])){
				?>
					<div class="error">Error<br/>
					<?php  
						echo ($_SESSION["error"]);
						unset($_SESSION["error"]);
					?>
					</div>
				<?php 	
				}
				if(!isset($_SESSION["userId"])){?>
				<form id='register' action='login.php' method='post' accept-charset='UTF-8'>
				<fieldset>
					<legend>Sign In</legend>
					<label for='username' >UserName<span style="color:red; font-size: 22px;">*</span>:</label>
					<input type='text' name='username' id='username' maxlength="50" />
					<br/>
					<br/>
					<label for='password' >Password<span style="color:red; font-size: 22px;">*</span>:</label>
					<input type='password' name='password' id='password' maxlength="50" />
					<br/>
					<br/>
					<input type='submit' name='authenticate' value='Submit' />
					<br/>
				</fieldset>
				</form>				
				<a href="registerView.php" id='register' style=" display: inline-block;">Not a user ? Register here !</a>
		<?php } else {	
			header("Location: dashboardView.php");
		} ?>
		</div>
	</body>
</html>