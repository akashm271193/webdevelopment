<html>
	<head>
		<title>Dashboard</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<?php 
			require_once 'dashboard.php';
			if(isset($_SESSION["message"])){
				?>
					<div class="message">
					<?php  
						echo($_SESSION["message"]);
						unset($_SESSION["message"]);
					?>
					</div>
			<?php }
			else if(isset($_SESSION["error"])){
				?>
					<div class="error"> Error <br/>
					<?php  
						echo($_SESSION["error"]);
						unset($_SESSION["error"]);
					?>
					</div>
			<?php }
			if(!isset($_SESSION["userId"])){
				header("Location: http://localhost:80/miniproject/loginView.php"); 
			}
			else { ?>
				<div id="main">
					<div id="dashboard">
						<form id='register' action='dashboard.php' method='POST' accept-charset='UTF-8'>
							<input style="float:right;width:20%" type="submit" value=" Logout" name="logout"/>
						</form>		
						<br/>
						<h1 style="text-align:center;margin-left: 20%;">DASHBOARD</h1>
						<form id='register' action='addRestaurant.php' method='GET' accept-charset='UTF-8'>
							<input style="width:20%" type="submit" value=" Add Restaurant " name="addRest"/>
						</form>
						<table id="dashtable" style="width:100%">
							<tbody id="dashtablebody">
								<tr id="rowTemplate">
									<td></td>
								</tr>
							</tbody>
						</table>
						<p id="tableError" colspan="5" style="text-align:center">No restaurants/reviews added. Add your first today <img src="https://html5-editor.net/images/smiley.png" alt="smiley" /> ! 
					</div>
				</div>				
		<?php } ?>
	</body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="js/dashboard.js"></script>
</html>