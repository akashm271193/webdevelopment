<?php 
require_once 'include/BaseModel.php';

/**
  * class UsersModel
  * extends BaseModel class present in include folder
  * Consists of fucntions to interact with user table in the db.
  **/
class UsersModel extends BaseModel {
	
	
	public function __construct(){
		parent::__construct();
		$this->dbprefix = $this->dbconfiguration["database"];			
		$this->usersTable = "users";
	}
	
	/**
	  * Function to authenticate user
	  * Checks if username is present or not
	  * If present, then verifies provided password with hashed password using
	  * PHP built-in fucntion php_verify()
	  * Parameters : username and password
	  * Returns: userId if authenticated and null - if authentication fails
	  **/
	public function authenticateUser($username,$password){
		$result = $this->db->query("Select * from ".$this->usersTable." where username = '".$username."'");
		if($result->num_rows == 0 || $result == false){
			return null;
		}
		else{
			$result = $result->fetch_array();
			if(password_verify($password,$result['password'])){
				return $result['id'];
			}
			else
				return null;
		}
	}
	
	/**
	  * Function to register user
	  * Parameters : Associative Array of user data consisting of - username,password,email,name
	  * Returns: userId if inserted successfully and false - if insertion fails
	  **/
	public function registerUser($data){
		$result = $this->db->query("INSERT into ".$this->usersTable."(username,password,name,email) VALUES ('".$data['username']."','".$data['password']."','".$data['name']."','".$data['email']."')");
		if($result == true){
			return $this->db->insert_id;
		}
		return $result;
	}
}	
?>
