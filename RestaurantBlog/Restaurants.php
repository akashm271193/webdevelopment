<?php 
require_once 'RestaurantsModel.php';
session_start();

/**
  * class Restaurants
  * Consists of functions to add reviews and restaurants
  * Consists of only one field -> restaurant Model class object
  **/
class Restaurants{
	protected $restaurantsModel;
	
	public function __construct(){
		$this->restaurantsModel = new RestaurantsModel();
	}
	
	/**
	  * Function to get reviews by user Id from restaurants model object.
	  * Gets user id from Session variable
	  * Returns: Array of restaurants consisting of restaurant name,location, id and array of review object consisting of review
	  * and updated timestamp
	  **/
	public function getReviewsByUserId(){
		$userId = $_SESSION["userId"];
		$result = $this->restaurantsModel->getRestaurantsAndReviewsByUserId($userId);
		return $result;
	}
	
	
	/**
	  * Function to get all reviews and restaurants from restaurants model object.
	  * Gets user id from Session variable
	  * Returns: Array of restaurants consisting of restaurant name,location, id and array of review object consisting of review
	  * and updated timestamp
	  **/
	public function getAllReviewsAndRestaurants(){
		$result = $this->restaurantsModel->getAllRestaurantsAndReviews();
		return $result;
	}
	
	/**
	  * Function to add restaurant
	  * if review field is present, it inserts into review table as well.
	  * Parameters : Associative Array of user data consisting of - restaurant name,restaurant location,userId 
	  * Returns: Id restaurant already exists, it returns back to add Restaurant page 
	  **/
	public function addRestaurant($data){
		$restaurantData['name'] = addslashes($data['name']);
		$restaurantData['location'] = addslashes($data['location']);
		$restaurantData['userId'] = $data['userId'];
		$restaurantId = $this->restaurantsModel->addRestaurant($restaurantData);
		if($restaurantId == -1){
			return -1;
		}
		else if($restaurantId != null && $restaurantId != false){
			if(isset($data['review'])){
				$reviewData['userId'] = $data['userId'];
				$reviewData['restaurantId'] = $restaurantId;
				$reviewData['review'] = $data['review'];
				$result = $this->restaurantsModel->addReview($reviewData);
				return $result;
			}
			return true;
		}
		return false;
	}
	
	/**
	  * Function to add review using restaurants model object
	  * Parameters : Associative Array consisting of userId, restaurantId and review  
	  * Returns: true - if inserted successfully or false - if fails 
	  **/
	public function addReview($data){
		$data['review'] = $data['review'];
		$isAdded = $this->restaurantsModel->addReview($data);
		return $isAdded;
	}
}
?>
