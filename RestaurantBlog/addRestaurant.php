<html>
	<head>
		<title>Add Restaurant</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>
		<script src="js/common.js"></script>
	</head>
	<body>
		<div id="main">
			<div id="restaurant">			
				<?php session_start();
					if(!isset($_SESSION['userId'])){
						header("Location: http://localhost:80/miniproject/loginView.php");
					} 
					if(isset($_SESSION["error"])){
				?>
					<div class="error">Error<br/>
					<?php  
						echo($_SESSION["error"]);
						unset($_SESSION["error"]);
					?>
					</div>
				<?php } ?>
				<input style="width:30%" type="submit" value=" << Go To Dashboard" name="back" id="backButton" onclick="backToDashboard()"/>
				<form id='addRestaurant' action='dashboard.php' method='post' accept-charset='UTF-8'>
					<fieldset>
						<legend>Add a Restaurant</legend>
						<br/>
						<label for='name' >Restaurant Name<span style="color:red; font-size: 22px;">*</span>: </label>
						<input type='text' name='name' id='name' maxlength="50" />
						<br/>
						<br/>
						<label for='location' >Restaurant Location<span style="color:red; font-size: 22px;">*</span>:</label>
						<input type='text' name='location' id='location' maxlength="50" />
						<br/>
						<br/>
						<label for='review' >Review:</label>
						<input type='text' name='review' id='review' maxlength="500" style="height:200px"/>
						<br/>
						<br/>
						<input type='submit' name='addRestaurant' value='Submit' />
					</fieldset>
				</form>
			</div>
		</div>
	</body>
</html>