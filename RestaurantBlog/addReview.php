<html>
	<head>
		<title>Add Review</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>
		<script src="js/common.js"></script>
	</head>
	<body>
		<div id="main">
			<div id="airport">
				<?php 	//error_reporting(0);
					if(isset($_POST["error"])){
				?>
					<div class="error"> Error <br/>
						<?php  
							echo($_POST["error"]);
							unset($_POST["error"]);
						?>
					</div>
				<?php }
					if(isset($_POST['restaurantId'])){?>
				<input style="width:30%" type="submit" value=" << Go To Dashboard" name="back" id="backButton" onclick="backToDashboard()"/>
				<form id='addReview' action='dashboard.php' method='post' accept-charset='UTF-8'>
					<fieldset>
						<legend>Add a Review</legend>
						<br/>
						<input type="hidden" name="restaurantId" value="<?php echo $_POST['restaurantId']; ?>"/>
						<input type="hidden" name="restaurantName" value="<?php echo $_POST['restaurantName']; ?>"/>
						<label for='name' >Restaurant Name<span style="color:red; font-size: 22px;">*</span>: </label>
						<input readonly type='text' name='name' id='name' value="<?php echo $_POST['restaurantName'];?>" maxlength="50"/>
						<br/>
						<br/>
						<label for='review' >Review<span style="color:red; font-size: 22px;">*</span>:</label>
						<input type='text' name='review' id='review' maxlength="500" style="height:200px"/>
						<br/>
						<br/>
						<input type='submit' name='addReview' value='Submit' />
					</fieldset>
				</form>
				<?php } else { header("Location: http://localhost:80/miniproject/addRestaurant.php");}?>
			</div>
		</div>
	</body>
</html>