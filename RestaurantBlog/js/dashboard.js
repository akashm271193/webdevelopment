$(document).ready(function(){
	//console.log("Ready");
	$("#tableError").hide();
	$("#rowTemplate").hide();
	getTableData();
	
});

function processTableData(data){
	var result = $.parseJSON(data);
	console.log(result);
	if(Array.isArray(result)){
		var id = 0;
		var tr  = $("#rowTemplate");
		var toAddAfter = $("#rowTemplate");
		result.forEach(function(restaurant) {
			var newRow = tr.clone();
			newRow.attr('id',"row"+id);
			tr.after(newRow);
			newRow.show();
			var newRowId = "#"+newRow.attr("id");
			var toDisplay = '<td><h1 id="restaurantName" style="text-align:center">'+ restaurant.restName+','+ restaurant.restLocation+'</h1><br/>'+
							'<h4>Reviews</h4>';
			if(restaurant.reviews != undefined && Array.isArray(restaurant.reviews)){
				restaurant.reviews.forEach(function(review){
					toDisplay += '<h3> -----> '+ review.timestamp+'</h3><p style="text-indent:30px;">"'+review.review+'"</p><br/><p> Review By -'+review.username+'</p>'
				});
			}
			toDisplay += "<form id='addRev' action='addReview.php' method='post' accept-charset='UTF-8'>"+
								'<input type="hidden" value="'+ restaurant.restName+'" name="restaurantName"/>'+
								'<input type="hidden" value="'+ restaurant.restId+'" name="restaurantId"/>'+
								'<input style="align:right;width:20%" type="submit" value=" Add Review " name="addReview"/>'+
						"</form></td>";
			$(newRowId).html(toDisplay);
			toAddAfter = newRow.attr("id");
			
		});	
	}
	else if(result == null){
		$("#tableError").show();
		$("#dashTable").hide();
	}
	else{
		
	}
};

function getTableData(){
	$.get("http://localhost:80/miniproject/browserCalls.php/getAllRestaurantsAndReviews",processTableData);
};

