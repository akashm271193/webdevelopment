<?php 
error_reporting(0);
require_once 'Users.php';
require_once 'Restaurants.php';

/**
  * Check if UserId is set in session variable.
  * IF not, then either session has expired, or someone is trying direct access
  * and throws the user back to login page.
  * Returns: null
  **/
if(!isset($_SESSION['userId'])){
	header("Location: http://localhost:80/miniproject/loginView.php");
}

/**
  * Check for the request method coming in
  * and based on the url structure, call the appropriate method
  * Returns: null
  **/
if($_SERVER['REQUEST_METHOD'] == 'GET'){
	$apis = new Apis();
	$pathParts = explode("/",$_SERVER['REQUEST_URI']);
	if(sizeof($pathParts) > 4){
		//TODO
	}
	else{
		$apis->{$pathParts[3]}();
	}
}

/**
  * class Apis
  * Consists of fucntions either to be given as an api or open functions for broswer calls.
  **/
class Apis{
	protected $restaurants;
	
	public function __construct(){
		$this->restaurants = new Restaurants();
		
	}
	
	/**
	  * Function to get a list of Restaurants and reviews added by a user
	  * Gets username from session variable and calls restaurant model object
	  * Checks for userId in session and if not present redirects to login
	  * Parameters : null
	  * Returns: Array of objects encoded as JSON. 
	  **/
	public function getRestaurantsAndReviews(){	
		if(isset($_SESSION['userId'])){
			$result = $this->restaurants->getReviewsByUserId();
			echo json_encode($result);
		}
		else{
			header("Location: http://localhost:80/miniproject/loginView.php");
		}
	}
	
	/**
	  * Function to get a list of Restaurants and reviews added by a user
	  * Gets username from session variable and calls restaurant model object
	  * Checks for userId in session and if not present redirects to login
	  * Parameters : null
	  * Returns: Array of objects encoded as JSON. 
	  **/
	public function getAllRestaurantsAndReviews(){	
		if(isset($_SESSION['userId'])){
			$result = $this->restaurants->getAllReviewsAndRestaurants();
			echo json_encode($result);
		}
		else{
			header("Location: http://localhost:80/miniproject/loginView.php");
		}
	}
}
?>
