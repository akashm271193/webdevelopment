<?php 
require_once 'UsersModel.php';
session_start();

/**
  * class Users
  * Consists of fucntions to authenticate and register user
  * Consists of only one field -> user Model class object
  **/
class Users {
	protected $usersModel;
	
	public function __construct(){
		$this->usersModel = new UsersModel();
	}
	
	/**
	  * Function to authenticate user using User model class
	  * Adds user Id to session if authenticated
	  * Parameters : username and password
	  * Returns: true - if authenticated and false - if authentication fails
	  **/
	public function authenticateUser($username,$password){
		$result = $this->usersModel->authenticateUser($username,$password);
		if($result == null){
			return false;
		}
		
		$_SESSION['userId'] = $result;
		return true;
	}
	
	/**
	  * Function to register user using User model class
	  * Adds user Id to session if successfully registered
	  * Parameters : Associative Array of user data consisting of - username,password,email,name 
	  * Returns: user id - if successfully registered and false - if registration fails
	  *
	  **/
	public function registerUser($data){
		$data['password'] = $this->encryptPassword($data['password']);
		if($data['password'] == null ||$data['password'] == false || $data['password'] == ""){
			return false;
		}
		$result = $this->usersModel->registerUser($data);
		$_SESSION['userId'] = $result;
		return $result;
	}
	
	/**
	  * Private function to encrypt the password using PHP built-in function password_hash()
	  * Parameters : password 
	  * Returns: Hashed password
	  **/
	private function encryptPassword($password){
		$encryptedPassword = password_hash($password,PASSWORD_DEFAULT);
		return $encryptedPassword;
	}
}	
?>
