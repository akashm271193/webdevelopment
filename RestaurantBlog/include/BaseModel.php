<?php 
require_once "configuration.php";

/**
  * class BaseModel
  * Acts as a core Model for other model classes.
  * Opens up a connection with mysql and stores it in its protected variable
  **/
class BaseModel{	
	protected $db;
	protected $dbconfiguration;
	
	protected function __construct(){
		global $dbconfig;	
		$this->dbconfiguration = $dbconfig;
		self::setupConnection();
	}
	
	/**
	  * Function to setup a connection with mysql db
	  * Gets configuration from 'configuration.php' file
	  * Parameters : null
	  * Returns: null
	  **/
	private function setupConnection(){
		$this->db = new mysqli($this->dbconfiguration['host'], $this->dbconfiguration['username'], $this->dbconfiguration['password'], $this->dbconfiguration['database']);
		if (!$this->db) {
			echo "Error: Unable to connect to MySQL." . PHP_EOL;
			echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
			echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
			exit;
		}
	}
	
}
?>