<?php 
require_once 'include/BaseModel.php';

/**
  * class RestaurantsModel
  * extends BaseModel class present in include folder
  * Consists of fucntions to interact with users,restaurants and reviews table in the db.
  **/
class RestaurantsModel extends BaseModel {
	
	public function __construct(){
		parent::__construct();
		$this->dbprefix = $this->dbconfiguration["database"];			
		$this->usersTable = "users";
		$this->restaurantsTable = "restaurants";
		$this->reviewsTable = "reviews";
	}
	
	/**
	  * Function to get Restaturant and reviews posted by a user
	  * Parameters : userId
	  * Returns: Array of restaurants consisting of restaurant name,location, id and array of review object consisting of review
	  * and updated timestamp
	  *
	  **/
	public function getRestaurantsAndReviewsByUserId($userId){
		$result = $this->db->query("Select rst.id,rst.name,rst.location,rt.review,rt.updatedTime from ".$this->restaurantsTable." rst left join ".$this->reviewsTable." rt on rt.restaurantId = rst.id where rst.userId = '".$userId."' ORDER BY rst.id ASC,rt.createdTime DESC");
		if($result->num_rows == 0){
			return null;
		}
		else{
			$currentId = 0;
			$data = array();
			$resultData	= array();		
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				if($row['id'] != $currentId || $currentId == 0){
					if($currentId != 0){
						array_push($resultData,$data);
					}
					$data = array();
					$data['restId'] = $row['id'];
					$data['restName'] = $row['name'];
					$data['restLocation'] = $row['location'];
				}
				if(isset($row['review']) && $row['review'] != null && $row['review'] != ""){
					if($row['id'] != $currentId){
						$data['reviews'] = array();
					}
					$reviewArr['review']= stripslashes($row['review']);
					$reviewArr['timestamp'] = $row['updatedTime'];
					array_push($data['reviews'],$reviewArr);
				}
				$currentId = $row['id'];
			}
			array_push($resultData,$data);
			return $resultData;
		}
	}
	
	
		/**
	  * Function to get all Restaturant and reviews posted by all the users
	  * Returns: Array of restaurants consisting of restaurant name,location, id and array of review object consisting of review
	  * and updated timestamp
	  *
	  **/
	public function getAllRestaurantsAndReviews(){
		$result = $this->db->query("Select rst.id,rst.name,rst.location,rt.review,rt.updatedTime,ut.username from ".$this->restaurantsTable." rst left join ".$this->reviewsTable." rt on rt.restaurantId = rst.id join ".$this->usersTable." ut on rt.userId = ut.id ORDER BY rst.id ASC,rt.createdTime DESC");
		if($result->num_rows == 0){
			return null;
		}
		else{
			$currentId = 0;
			$data = array();
			$resultData	= array();		
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				if($row['id'] != $currentId || $currentId == 0){
					if($currentId != 0){
						array_push($resultData,$data);
					}
					$data = array();
					$data['restId'] = $row['id'];
					$data['restName'] = $row['name'];
					$data['restLocation'] = $row['location'];
				}
				if(isset($row['review']) && $row['review'] != null && $row['review'] != ""){
					if($row['id'] != $currentId){
						$data['reviews'] = array();
					}
					$reviewArr['review']= stripslashes($row['review']);
					$reviewArr['timestamp'] = $row['updatedTime'];
					$reviewArr['username'] = $row['username'];
					array_push($data['reviews'],$reviewArr);
				}
				$currentId = $row['id'];
			}
			array_push($resultData,$data);
			return $resultData;
		}
	}
	
	/**
	  * Function to add Restaturant
	  * It checks if the same restaurant has been added by the user or not
	  * If not, makes a new entry in the restaurants table.
	  * Parameters : Data array consisting of restaurant name, location and user Id . 
	  * Returns: restaurant id if insertion is successful, -1 if restaurant is already present and false - if insertion fails
	  * and updated timestamp
	  *
	  **/
	public function addRestaurant($data){
		$result = $this->db->query("Select id from ".$this->restaurantsTable." where name='".$data['name']."' and location='".$data['location']."' and userId=".$data['userId']."");
		if($result->num_rows >0){
			return -1;
		}
		
		$result = $this->db->query("INSERT into ".$this->restaurantsTable."(name,location,userId) VALUES ('".$data['name']."','".$data['location']."',".$data['userId'].")");
		if($result == true){
			return $this->db->insert_id;
		}
		return $result;
	}
	
	/**
	  * Function to add review table
	  * Parameters : data array consisting of userId, restaurantId and review 
	  * Returns: true - if inserts successfully and false - if insertion fails
	  *
	  **/
	public function addReview($data){
		$result = $this->db->query("INSERT into ".$this->reviewsTable."(userId,restaurantId,review) VALUES ('".$data['userId']."','".$data['restaurantId']."','".$data['review']."')");
		return $result;
	}
}	
?>
