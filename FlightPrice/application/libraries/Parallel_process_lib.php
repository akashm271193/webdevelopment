<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/*error_reporting(E_ALL);
ini_set('display_errors',1);*/
//error_reporting(-1);
class Parallel_process_lib
{
    public function __construct() 
    {
        $this->ci =& get_instance();
		$this->ci->load->library('logging_lib',null,"logger");
    }
	public function doInBackground($url, $params = null) {
		try{
			$cmd = 'curl -X GET -H "Content-Type: application/json"';
			$cmd.= ' "' .$url. '"';
			$cmd .= " > /dev/null 2>&1 &";

			exec($cmd, $output, $exit);
			return $exit == 0;
		} catch (Exception $ex){
			$msg = "Parallel process lib - Error while making a curl call : ".$ex->getMessage(). " :::::::: for url :".$url.PHP_EOL;
			$this->ci->logger->logError($msg,"prices");
		}
	}
}
?>
