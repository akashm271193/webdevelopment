<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
	Class to send push notifications using Google Cloud Messaging for Android
	Example usage
	-----------------------
	$an = new GCMPushMessage($apiKey);
	$an->setDevices($devices);
	$response = $an->send($message);
	-----------------------
	
	$apiKey Your GCM api key
	$devices An array or string of registered device tokens
	$message The mesasge you want to push out
	@author Matt Grundy
	Adapted from the code available at:
	http://stackoverflow.com/questions/11242743/gcm-with-php-google-cloud-messaging
*/
class Sky_scanner_api_lib {
	
	public function __construct()
	{
		$this->ci =& get_instance();
		
		$this->serverApiKey = $this->ci->config->item('apiKey');
	}

	
	/*
		Send the message to the device
		@param $message The message to send
		@param $data Array of data to accompany the message
	*/
	public function createSession($data){
		try {
				
				$headers = array( 
					'Content-Type: application/x-www-form-urlencoded',
					'Accept: application/json'
				);
				// Open connection
				$ch = curl_init();
				$url = 'http://partners.api.skyscanner.net/apiservices/pricing/v1.0';
				// Set the url, number of POST vars, POST data
				curl_setopt( $ch, CURLOPT_URL, $url);
				// curl_setopt($ch, CURLOPT_PORT, 443);
				curl_setopt( $ch, CURLOPT_POST, true );
				curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);	
				curl_setopt( $ch, CURLOPT_POSTFIELDS,$data);
				
				// Avoids problem with https certificate
				curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
				
				//allow header to be included in resonse
				curl_setopt($ch, CURLOPT_HEADER, 1);

				// Execute post
				$response = curl_exec($ch);

				$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);

				$result['header'] = $this->explodeHeaders(substr($response, 0, $header_size));
				$result['body'] = substr($response, $header_size);
				$result['response'] = $response;
				curl_close($ch);
				// Close connection
				return $result;
			} catch (Exception $e) {
				$this->log(date('Y-m-d H:i:s')." Error while creating session in SkyScanner Lib : ".json_encode($e->getMessage()).PHP_EOL);
				return "0";
		}
	}

		/*
		Send the message to the device
		@param $message The message to send
		@param $data Array of data to accompany the message
	*/
	public function pollResults($sessionKey,$apiKey){
		try{
			$ch = curl_init();
			$url = $sessionKey.'?apikey='.$apiKey;
			// Set the url, number of POST vars, POST data
			curl_setopt( $ch, CURLOPT_URL, $url);
			// curl_setopt($ch, CURLOPT_PORT, 443);
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);	
			
			// Avoids problem with https certificate
			curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
			
			//allow header to be included in resonse
			//curl_setopt($ch, CURLOPT_HEADER, 1);

			// Execute post
			$response = curl_exec($ch);

			/*$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);

			$result['header'] = $this->explodeHeaders(substr($response, 0, $header_size));
			$result['body'] = substr($response, $header_size);
			$result['response'] = $response;*/
			curl_close($ch);
			// Close connectioon
			return $response;
		} catch(Exception $e){
			$this->log(date('Y-m-d H:i:s')." Error while pollingResults in SkyScanner Lib : ".json_encode($e->getMessage()).PHP_EOL);
			return "0";
		}
	}

	public function explodeHeaders($headerContent)
	{

	    $headers = array();

	    // Split the string on every "double" new line.
	    $arrRequests = explode("\r\n\r\n", $headerContent);

	    // Loop of response headers. The "count() -1" is to 
	    //avoid an empty row for the extra line break before the body of the response.
	    for ($index = 0; $index < count($arrRequests) -1; $index++) {

	        foreach (explode("\r\n", $arrRequests[$index]) as $i => $line)
	        {
	            if ($i === 0)
	                $headers[$index]['http_code'] = $line;
	            else
	            {
	                list ($key, $value) = explode(': ', $line);
	                $headers[$index][$key] = $value;
	            }
	        }
	    }

	    return $headers;
	}
	
	function log($msg)
   	{
          $location  = FCPATH.'application/logs/'.date("Y-m-d").'_SkyScannerLib_logs.json';
	 	  $myfile = fopen($location, "a");
          fwrite($myfile, $msg);
          fclose($myfile);
          return true ;
	}
}
