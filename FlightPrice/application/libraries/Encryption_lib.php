<?php
/*
	Class to encrypt data

*/
class Encryption_lib {
	var $url ;
	var $serverApiKey;
	var $devices = array();
	
	
	function __construct()
	{
		$this->ci =& get_instance();
	}

	/*
		Constructor
		Function to create a seed for encryption.
	*/
	function createSeed(){
		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
		$result = '';
		for ($i = 0; $i < 10; $i++)
			$result .= $characters[mt_rand(0, 61)];
		
		return $result;
	}
	/*
		Encrypt the data using 
		@param $data Data to be encypted
	*/
	function hashEncrypt($data){
		return hash('sha512', $data);
	}
}