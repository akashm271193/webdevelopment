<html>
	<head>
		<title>Add/Delete Routes</title>
		<link rel="stylesheet" type="text/css" href="
			<?php echo base_url(); ?>/assets/css/style.css">
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<div id="main">
			<div id="routes">
				<h1 style="text-align:center">ROUTES</h1>
				<?php echo form_open('travelRoutes/openAddRoute'); ?>
					<input style="align:right;width:20%" type="submit" value=" Add Route " name="addroute"/>
				<?php echo form_close(); ?>
				<table style="width:100%">
					<thead>
						<tr>
							<th>Id </br> <input type="text" name="searchRouteId" id="searchRouteId" placeholder="ID"/></th>
							<th>From Airport </br><input type="text" name="searchRouteFromCode" id="searchRouteFromCode" placeholder="From Airport"/></th>
							<th>To Airport </br> <input type="text" name="searchRouteToCode" id="searchRouteToCode" placeholder="To Airport"/></th>
							<th>Creation Time</th>
							<th>Update Time</th>
							<th> <input type="submit" value=" Search " name="search"/></th>
						</tr>	
					</thead>
					<tbody>
						<?php if($routeCount == 0) { ?>
							<tr> <td colspan="5" style="text-align:center">No route added to RouteMaster. Please add a Route first ! </td></tr>
						<?php } else { 
							for($i = 0; $i < $routeCount ; $i++){?>
							<tr>
								<?php echo form_open('travelRoutes/removeRoute'); ?>
									<td><input type="hidden" name="id" value="<?php echo $routeMaster[$i]['id'] ?>" /><?php echo $routeMaster[$i]['id'] ?></td>
									<td><?php echo $routeMaster[$i]['fromAirportName']." (".$routeMaster[$i]['fromAirportCode'].")" ?></td>
									<td><?php echo $routeMaster[$i]['toAirportName']." (".$routeMaster[$i]['toAirportCode'].")" ?></td>
									<td><?php echo $routeMaster[$i]['creationTime'] ?></td>
									<td><?php echo $routeMaster[$i]['updateTime'] ?></td>
									<td><input style="align:center" type="submit" value="Delete" name="deleteAirport"/></td>
								<?php echo form_close(); ?>
							</tr>
						<?php }}?>
					</tbody>
				</table>
			</div>
		</div>
	</body>
</html>