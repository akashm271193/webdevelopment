<html>
	<head>
		<title>Add Route</title>
		<link rel="stylesheet" type="text/css" href="
			<?php echo base_url(); ?>/assets/css/style.css">
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<div id="main">
			<div id="route">
				<h1 style="text-align:center">ADD A TRAVEL ROUTE</h1>
				<?php echo form_open('travelRoutes/addRoute'); ?>
				<label>From Airport :</label>
				<select name="fromAirportId" id = "fromAirportId">
					<option value="">Select A Start Point</option>
					<?php for($i = 0 ; $i < $airportCount ; $i++) {?>
					<option value="<?php echo $airportMaster[$i]['id']; ?>"><?php echo $airportMaster[$i]['airportName']." (".$airportMaster[$i]['airportCode'].")"; ?></option>
					<?php } ?>
				</select><br /><br />
				<label>To Airport :</label>
				<select name="toAirportId" id = "toAirportId">
					<option value="">Select A Destination</option>
					<?php for($i = 0 ; $i < $airportCount ; $i++) {?>
					<option value="<?php echo $airportMaster[$i]['id']; ?>"><?php echo $airportMaster[$i]['airportName']." (".$airportMaster[$i]['airportCode'].")"; ?></option>
					<?php } ?>
				</select><br /><br />
				<input type="submit" value=" Add Airport " name="submit"/><br />
				<?php echo form_close(); ?>
			</div>
		</div>
	</body>
</html>