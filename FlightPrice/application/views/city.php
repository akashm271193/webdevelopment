<html>
	<head>
		<title>Add/Delete City</title>
		<link rel="stylesheet" type="text/css" href="
			<?php echo base_url(); ?>/assets/css/style.css">
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<div id="main">
			<div id="city">
				<h1 style="text-align:center">CITIES</h1>
				<?php echo form_open('city/openAddCity'); ?>
					<input style="align:right;width:20%" type="submit" value=" Add City " name="addcity"/>
				<?php echo form_close(); ?>
				<table style="width:100%">
					<thead>
						<tr>
							<th>Id </br> <input type="text" name="searchCityId" id="searchCityId" placeholder="ID"/></th>
							<th>City Name </br><input type="text" name="searchCityName" id="searchCityName" placeholder="City Name"/></th>
							<th>City Code </br> <input type="text" name="searchCityCode" id="searchCityCode" placeholder="City Code"/></th>
							<th>Creation Time</th>
							<th>Update Time</th>
							<th> <input type="submit" value=" Search " name="search"/></th>
						</tr>	
					</thead>
					<tbody>
						<?php if($cityCount == 0) { ?>
							<tr> <td colspan="5" style="text-align:center">No city added to CityMaster. Please add a City first ! </td></tr>
						<?php } else { 
							for($i = 0; $i < $cityCount ; $i++){?>
							<tr>
								<?php echo form_open('city/removeCity'); ?>
									<td><input type="hidden" name="id" value="<?php echo $cityMaster[$i]['id'] ?>" /><?php echo $cityMaster[$i]['id'] ?></td>
									<td><?php echo $cityMaster[$i]['cityName'] ?></td>
									<td><input type="hidden" name="cityCode" value="<?php echo $cityMaster[$i]['cityCode'] ?>" /><?php echo $cityMaster[$i]['cityCode'] ?></td>
									<td><?php echo $cityMaster[$i]['creationTime'] ?></td>
									<td><?php echo $cityMaster[$i]['updateTime'] ?></td>
									<td><input style="align:center" type="submit" value="Delete" name="deleteCity"/></td>
								<?php echo form_close(); ?>
							</tr>
						<?php }}?>
					</tbody>
				</table>
			</div>
		</div>
	</body>
</html>