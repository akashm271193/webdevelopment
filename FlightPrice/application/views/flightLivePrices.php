<html>
	<head>
		<title>Get Live Prices</title>
		<link rel="stylesheet" type="text/css" href="
			<?php echo base_url(); ?>/assets/css/style.css">
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<div id="main">
			<div id="city">
				<h1 style="text-align:center">GET LIVE Prices</h1>
				<?php echo form_open('FlightLivePrices/getLivePrices'); ?>
					<label>Origin Airport :</label>
					<input type="text" name="originAirport" id="originAirport" placeholder="Name of the Origin Airport"/><br /><br />
					<label>Destination Airport</label>
					<input type="text" name="destinationAirport" id="destinationAirport" placeholder="Name of the Destination Airport"/><br /><br />
					<label>Start Date</label>
					<input type="text" name="startDate" id="startDate" placeholder="Start Date"/><br /><br />
					<label>End Date</label>
					<input type="text" name="endDate" id="endDate" placeholder="End Date"/><br /><br />
					<input type="submit" value="Get Prices" name="submit"/><br />
				<?php echo form_close(); ?>
			</div>
		</div>
	</body>
</html>