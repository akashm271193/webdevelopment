<html>
	<head>
		<title>Add City</title>
		<link rel="stylesheet" type="text/css" href="
			<?php echo base_url(); ?>/assets/css/style.css">
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<div id="main">
			<div id="city">
				<h1 style="text-align:center">ADD A CITY</h1>
				<?php echo form_open('city/addCity'); ?>
				<label>City Name :</label>
				<input type="text" name="cityName" id="cityName" placeholder="Name of the City"/><br /><br />
				<label>City Code :</label>
				<input type="text" name="cityCode" id="cityCode" placeholder="Code for the City"/><br /><br />
				<input type="submit" value=" Add City " name="submit"/><br />
				<?php echo form_close(); ?>
			</div>
		</div>
	</body>
</html>