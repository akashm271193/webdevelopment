<html>
	<head>
		<title>Add Airport</title>
		<link rel="stylesheet" type="text/css" href="
			<?php echo base_url(); ?>/assets/css/style.css">
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<div id="main">
			<div id="airport">
				<h1 style="text-align:center">ADD AN AIRPORT</h1>
				<?php echo form_open('airport/addAirport'); ?>
				<label>City :</label>
				<select name="cityId" id = "cityId">
					<option value="">Select A City</option>
					<?php for($i = 0 ; $i < $cityCount ; $i++) {?>
					<option value="<?php echo $cityMaster[$i]['id']; ?>"><?php echo $cityMaster[$i]['cityName']; ?></option>
					<?php } ?>
				</select><br /><br />
				<label>Airport Name :</label>
				<input type="text" name="airportName" id="airportName" placeholder="Name of the Airport"/><br /><br />
				<label>Airport Code :</label>
				<input type="text" name="airportCode" id="airportCode" placeholder="Code for the Airport"/><br /><br />
				<input type="submit" value=" Add Airport " name="submit"/><br />
				<?php echo form_close(); ?>
			</div>
		</div>
	</body>
</html>