<html>
	<head>
		<title>Add/Delete Airport</title>
		<link rel="stylesheet" type="text/css" href="
			<?php echo base_url(); ?>/assets/css/style.css">
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<div id="main">
			<div id="airport">
				<h1 style="text-align:center">AIRPORTS</h1>
				<?php echo form_open('airport/openAddAirport'); ?>
					<input style="align:right;width:20%" type="submit" value=" Add Airport " name="addAirport"/>
				<?php echo form_close(); ?>
				<table style="width:100%">
					<thead>
						<tr>
							<th>Id </br> <input type="text" name="searchAirportId" id="searchAirportId" placeholder="ID"/></th>
							<th>City </br> <input type="text" name="searchCityName" id="searchCityName" placeholder="City Name"/></th>
							<th>Airport Name </br><input type="text" name="searchAirportName" id="searchAirportName" placeholder="Airport Name"/></th>
							<th>Airport Code </br> <input type="text" name="searchAirportCode" id="searchAirportCode" placeholder="Airport Code"/></th>
							<th>Creation Time</th>
							<th>Update Time</th>
							<th> <input type="submit" value=" Search " name="search"/></th>
						</tr>	
					</thead>
					<tbody>
						<?php if($airportCount == 0) { ?>
							<tr> <td colspan="5" style="text-align:center">No airport added to AirportMaster. Please add a Airport first ! </td></tr>
						<?php } else { 
							for($i = 0; $i < $airportCount ; $i++){?>
							<tr>
								<?php echo form_open('airport/removeAirport'); ?>
									<td><input type="hidden" name="id" value="<?php echo $airportMaster[$i]['id'] ?>" /><?php echo $airportMaster[$i]['id'] ?></td>
									<td><input type="hidden" name="cityId" value="<?php echo $airportMaster[$i]['cityId'] ?>" /><?php echo $airportMaster[$i]['cityName'] ?></td>
									<td><?php echo $airportMaster[$i]['airportName'] ?></td>
									<td><input type="hidden" name="airportCode" value="<?php echo $airportMaster[$i]['airportCode'] ?>" /><?php echo $airportMaster[$i]['airportCode'] ?></td>
									<td><?php echo $airportMaster[$i]['creationTime'] ?></td>
									<td><?php echo $airportMaster[$i]['updateTime'] ?></td>
									<td><input style="align:center" type="submit" value="Delete" name="deleteAirport"/></td>
								<?php echo form_close(); ?>
							</tr>
						<?php }}?>
					</tbody>
				</table>
			</div>
		</div>
	</body>
</html>