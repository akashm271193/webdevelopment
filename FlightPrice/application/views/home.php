<html>
	<head>
		<title>Home</title>
		<link rel="stylesheet" type="text/css" href="
			<?php echo base_url(); ?>/assets/css/style.css">
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<div id="main">
			<div id="airport">
				<h1 style="text-align:center">Home</h1>
				<?php echo form_open('airport'); ?>
					<input style="align:right;width:20%" type="submit" value=" View Airports " name="viewAirports"/>
				<?php echo form_close(); ?>
				<?php echo form_open('city'); ?>
					<input style="align:right;width:20%" type="submit" value=" View Cities " name="viewCities"/>
				<?php echo form_close(); ?>
				<?php echo form_open('travelRoutes'); ?>
					<input style="align:right;width:20%" type="submit" value=" View Routes " name="viewRoutes"/>
				<?php echo form_close(); ?>
			</div>
		</div>
	</body>
</html>