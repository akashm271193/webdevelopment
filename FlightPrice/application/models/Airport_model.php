<?php
/*error_reporting(E_ALL);
ini_set('display_errors',1);*/

class Airport_model extends CI_Model
{
    public $string;

    public function __construct(){
		$ci =& get_instance();
    	$this->load->database('default');
		$this->airportMaster = "airportMaster";
		$this->cityMaster = "cityMaster";
    }
	
    /** @Akash-13/03/2017
	  * 
	  * Adds an airport to the Airport Master Table
	  * 
	  * @params (Data array having keys airportName and airportCode)
	  * @returns(1 if success, 0 if failed)
	  *	  
	  **/
	public function addAirport($data)
	{
		$result = $this->db->insert($this->airportMaster,$data);
		return $result;
	}


    /** @AK-13/03/2017
	  * 
	  * Remove an airport from the airportMaster and delete all its entries from Airport Master
	  * 
	  * @params (cityId , airportId and airportCode of the airport to be deleted)
	  * @returns(0 or 1)
	  **/
	public function removeAirport($data)
	{
		$result = $this->db->delete($this->airportMaster,array('id' => $data['id'],'cityId' => $data['cityId'],"airportCode" =>$data['airportCode']));
		return $result;
	}


	/** @AK-13/03/2017
	  * 
	  * Get all airports from AirportMaster Table
	  * 
	  * @params (null)
	  * @returns([array] of all rows in the airportMaster table)
	  **/
	public function getAllAirports()
	{
		$this->db->select('am.*,cm.id as cityId,cm.cityName');
		$this->db->join($this->cityMaster." as cm","am.cityId = cm.id");
		$this->db->order_by('am.id','desc');
		$result = $this->db->get($this->airportMaster." as am");
		/*print_r($result->result_array());
		die;*/
		return $result->result_array();
	}

    /** @AK-13/03/2017
	  * 
	  * Register the device using Gcm Device Id sent thrpugh the app
	  * 
	  * @params (registration_id ----> Registration id of the device, type ----- > insert(if device exists)(type = '0')/update(if new device id)(type = '1'))
	  * @returns(1 if success, 0 if failed)
	  **/
	public function updateAirport($data)
	{

		$this->db->select("unique_device_id",(string)$data['unique_device_id']);
		$result = $this->db->update($this->gcm_device_ids_table,$data);
		return $result;
	}


}
