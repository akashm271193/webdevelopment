<?php
/*error_reporting(E_ALL);
ini_set('display_errors',1);*/

class City_model extends CI_Model
{
    public $string;

    public function __construct(){
		$ci =& get_instance();
    	$this->load->database('default');
		$this->cityMaster = "cityMaster";
    }
	
    /** @Akash-12/03/2017
	  * 
	  * Adds a city to the City Master Table
	  * 
	  * @params (Data array having keys cityName and cityCode)
	  * @returns(1 if success, 0 if failed)
	  *	  
	  **/
	public function addCity($data)
	{
		$result = $this->db->insert($this->cityMaster,$data);
		return $result;
	}


    /** @Akash-12/03/2017
	  * 
	  * Remove a city from the cityMaster and delete all its entries from Airport Master
	  * 
	  * @params (city code and city id)
	  * @returns(Success or failure message)
	  **/
	public function removeCity($data)
	{
		$result = $this->db->delete($this->cityMaster,array('id' => $data['id'],"cityCode" =>$data['cityCode']));
		return $result;
	}


	/** @Akash-12/03/2017
	  * 
	  * Get all cities from CityMaster Table
	  * 
	  * @params (null)
	  * @returns([array] of all rows in the cityMaster table)
	  **/
	public function getAllCities()
	{
		$this->db->select('*');
		$this->db->order_by('id','desc');
		$result = $this->db->get($this->cityMaster);
		/*print_r($result->result_array());
		die;*/
		return $result->result_array();
	}

    /** @AK-18/01/2016
	  * 
	  * Register the device using Gcm Device Id sent thrpugh the app
	  * 
	  * @params (registration_id ----> Registration id of the device, type ----- > insert(if device exists)(type = '0')/update(if new device id)(type = '1'))
	  * @returns(1 if success, 0 if failed)
	  **/
	public function updateCity($data)
	{

		$this->db->select("unique_device_id",(string)$data['unique_device_id']);
		$result = $this->db->update($this->gcm_device_ids_table,$data);
		return $result;
	}


}