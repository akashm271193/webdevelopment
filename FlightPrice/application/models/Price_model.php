<?php
/*error_reporting(E_ALL);
ini_set('display_errors',1);*/

class Price_model extends CI_Model
{
    public function __construct(){
    	$this->load->library('sky_scanner_api_lib',null,"ssaLib");
    	$this->load->library('logging_lib',null,"logger");
    	$this->load->database('default');
    	$this->airportMaster = "airportMaster";
		$this->cityMaster = "cityMaster";
		$this->batchMaster= "batchMaster";
		$this->queryMaster= "queryMaster";
		$this->statusMaster= "statusMaster";
		$this->queryCurrencies= "queryCurrencies";
		$this->queryPlaces = "queryPlaces";
		$this->queryAgents= "queryAgents";
		$this->queryCarriers = "queryCarriers";
		$this->querySegments = "querySegments";
		$this->queryLegs = "queryLegs";
		$this->queryItineraryMaster = "queryItineraryMaster";
		$this->queryItineraryAgents = "queryItineraryAgents";
		$this->queryFailed = "queryFailed";
    }
	
    /** @Akash-14/03/2017
	  * 
	  * Adds a city to the City Master Table
	  * 
	  * @params (Data array having keys cityName and cityCode)
	  * @returns(1 if success, 0 if failed)
	  *	  
	  **/
	public function getLivePrices($data)
	{
		$postData = $this->convertToForm($data);
		$msg = "Prices Model - Data being posted to SkyScanner : ".json_encode($data).PHP_EOL;
		$this->logger->logInfo($msg,"prices");

		$result = $this->ssaLib->createSession($postData);
		if(strcasecmp($result['body'], '{}') != 0 || $result == "0")
		{
			$msg = "Prices Model - Error while creating session for data == ".json_encode($postData)." ===== Result from Lib CreateSession : ".json_encode($result).PHP_EOL;
			$this->logger->logError($msg,"prices");

			$fdResult = $this->insertFailedQuery($data);
			if ($fdResult == 1 ) {
					$msg = "Prices Model - Query inserted in  queryFailed : ".json_encode($data).PHP_EOL;
					$this->logger->logInfo($msg,"prices");

					return -1;
				}
				else {
					$msg = "Prices Model - Error in inserting query in  queryFailed: ".json_encode($data).PHP_EOL;
					$this->logger->logError($msg,"prices");
					return 0;
				}
		}
		else
		{
			$sessionId=$result['header'][0]['Location'];
			$msg = "Prices Model - Response from Session Id : ".$sessionId.PHP_EOL;
			$this->logger->logInfo($msg,"prices");		

			$response = $this->ssaLib->pollResults($sessionId,$data['apikey']);
			if($response == null || $response == "" || $response == " " || $response == "0")
			{
				$msg = "Prices Model - Error while getting response from polling result : ".$response. " ======= for data ".json_encode($postData).PHP_EOL;
				$this->logger->logError($msg,"prices");
				$fdResult = $this->insertFailedQuery($data);
				if ($fdResult == 1 ) {
					$msg = "Prices Model - Query inserted in  queryFailed: ".json_encode($data).PHP_EOL;
					$this->logger->logInfo($msg,"prices");
					return -1;
				}
				else {
					$msg = "Prices Model - Error in inserting query in  queryFailed: ".json_encode($data).PHP_EOL;
					$this->logger->logError($msg,"prices");
					return 0;
				}
			}
			else
			{
				$msg = "Prices Model - Response from polling result : ".substr($response,0,50)."......... }".PHP_EOL;
				$this->logger->logInfo($msg,"prices");
				$result = $this->parseLivePriceResponse($response,$data);
				return $result;
			}
		}	
		
	}

	/** @Akash-19/03/2017
	  * 
	  * Function to parse the response.
	  * 
	  * @params (Response string)
	  * @returns(1 if success, 0 if failed)
	  *	  
	  *
	  **/
	public function parseLivePriceResponse($response,$postData)
	{
		try{
			$this->decodedResponse = json_decode($response);
			
			$batchData['executionTime'] = date('Y-m-d H:i:s',time());
			$this->batchId = $this->insertIntoBatchMaster($batchData);
			$this->db->trans_begin();
			
			
			/** 
			  * Functionality to insert in Query Master
			  **/
			$queryData['batchId'] = $this->batchId;
			$queryData['sessionKey'] = $this->decodedResponse->SessionKey;
			$queryData['query'] = json_encode($this->decodedResponse->Query);
			$queryData['status'] = $this->getStatusCode($this->decodedResponse->Status);
			$queryMasterResult = $this->insertIntoQueryMaster($queryData);
			$queryId = $queryMasterResult['id'];
			if($queryId == "" || $queryId == null)
			{
				$msg = "Prices Model - Error in fetching id of queryMaster entry. Id fetched ==> ".$queryId.PHP_EOL;
				$this->logger->logError($msg,"prices");
				throw new Exception("Error in fetching id of queryMaster entry");
			}
			
			/** 
			  * Functionality to insert in Query Currencies
			  **/
			$dataArray = array();
			foreach ($this->decodedResponse->Currencies as $currency) {
				$currencyData['queryId'] = $queryId;
				$currencyData['currencyCode'] = $currency->Code;
				$currencyData['currencySymbol'] = $currency->Symbol;
				$currencyData['thousandsSeparator'] = $currency->ThousandsSeparator;
				$currencyData['decimalSeparator']= $currency->DecimalSeparator;
				$currencyData['symbolOnLeft']= $currency->SymbolOnLeft;
				$currencyData['spaceBetweenAmountAndSymbol']= $currency->SpaceBetweenAmountAndSymbol;
				$currencyData['roundingCoefficient']= $currency->RoundingCoefficient;
				$currencyData['decimalDigits']= $currency->DecimalDigits;
				array_push($dataArray, $currencyData);
			}
			if(sizeof($dataArray) > 0)
				$this->insertIntoQueryCurrencies($dataArray);
			else
			{
				$msg = "Prices Model - insertIntoQueryCurrencies - Error in dataArray ==> ".$json_encode($dataArray).PHP_EOL;
				$this->logger->logError($msg,"prices");
				throw new Exception("insertIntoQueryCurrencies - Error in dataArray");
			}

			/** 
			  * Functionality to insert in Query Places
			  **/
			$dataArray = array();
			foreach ($this->decodedResponse->Places as $place) {
				$placeData['queryId'] = $queryId;
				$placeData['placeId'] = $place->Id;
				$placeData['placeParentId'] = $place->ParentId;
				$placeData['placeCode'] = $place->Code;
				$placeData['placeType']= $place->Type;
				$placeData['placeName']= $place->Name;
				array_push($dataArray, $placeData);
			}
			if(sizeof($dataArray) > 0)
				$this->insertIntoQueryPlaces($dataArray);
			else
			{
				$msg = "Prices Model - insertIntoQueryPlaces - Error in dataArray ==> ".$json_encode($dataArray).PHP_EOL;
				$this->logger->logError($msg,"prices");
				throw new Exception("insertIntoQueryPlaces - Error in dataArray");
			}

			
			/** 
			  * Functionality to insert in Query Agents
			  **/
			$dataArray = array();
			foreach ($this->decodedResponse->Agents as $agent) {
				$agentData['queryId'] = $queryId;
				$agentData['agentId'] = $agent->Id;
				$agentData['agentName'] = $agent->Name;
				$agentData['agentImage'] = $agent->ImageUrl;
				$agentData['agentStatus']= $this->getStatusCode($agent->Status);
				$agentData['agentOptimisedForMobile']= $agent->OptimisedForMobile;
				$agentData['agentBookingNumber']= $agent->BookingNumber;
				$agentData['agentType']= $agent->Type;
				array_push($dataArray, $agentData);
			}
			if(sizeof($dataArray) > 0)
				$this->insertIntoQueryAgents($dataArray);
			else
			{
				$msg = "Prices Model - insertIntoQueryAgents - Error in dataArray ==> ".json_encode($dataArray).PHP_EOL;
				$this->logger->logError($msg,"prices");
				throw new Exception("insertIntoQueryAgents - Error in dataArray");
			}

			
			/** 
			  * Functionality to insert in Query Carriers
			  **/
			$dataArray = array();
			foreach ($this->decodedResponse->Carriers as $carrier) {
				$carrierData['queryId'] = $queryId;
				$carrierData['carrierId'] = $carrier->Id;
				$carrierData['carrierCode'] = $carrier->Code;
				$carrierData['carrierName'] = $carrier->Name;
				$carrierData['carrierImage']= $carrier->ImageUrl;
				$carrierData['carrierDisplayCode']= $carrier->DisplayCode;
				array_push($dataArray, $carrierData);
			}
			if(sizeof($dataArray) > 0)
				$this->insertIntoQueryCarriers($dataArray);
			else
			{
				$msg = "Prices Model - insertIntoQueryCarriers - Error in dataArray ==> ".json_encode($dataArray).PHP_EOL;
				$this->logger->logError($msg,"prices");
				throw new Exception("insertIntoQueryCarriers - Error in dataArray");
			}


			/** 
			  * Functionality to insert in Query Segments
			  **/
			$dataArray = array();
			foreach ($this->decodedResponse->Segments as $segment) {
				$segmentData['queryId'] = $queryId;
				$segmentData['segmentId'] = $segment->Id;
				$segmentData['originStation'] = $segment->OriginStation;
				$segmentData['destinationStation'] = $segment->DestinationStation;
				$segmentData['departureTime']= $segment->DepartureDateTime;
				$segmentData['arrivalTime']= $segment->ArrivalDateTime;
				$segmentData['carrierId'] = $segment->Carrier;
				$segmentData['operatingCarrierId'] = $segment->OperatingCarrier;
				$segmentData['duration'] = $segment->Duration;
				$segmentData['flightNumber']= $segment->FlightNumber;
				$segmentData['journeyMode']= $segment->JourneyMode;
				$segmentData['directionality']= $segment->Directionality;
				array_push($dataArray, $segmentData);
			}
			if(sizeof($dataArray) > 0)
				$this->insertIntoQuerySegments($dataArray);
			else
			{
				$msg = "Prices Model - insertIntoQuerySegments - Error in dataArray ==> ".json_encode($dataArray).PHP_EOL;
				$this->logger->logError($msg,"prices");
				throw new Exception("insertIntoQuerySegments - Error in dataArray");
			}

			
			/** 
			  * Functionality to insert in Query Legs
			  **/
			$dataArray = array();
			foreach ($this->decodedResponse->Legs as $leg) {
				$legData['queryId'] = $queryId;
				$legData['legId'] = $leg->Id;
				$legData['segmentIds'] = implode(",", $leg->SegmentIds);
				$legData['originStation'] = $leg->OriginStation;
				$legData['destinationStation'] = $leg->DestinationStation;
				$legData['departureTime']= $leg->Departure;
				$legData['arrivalTime']= $leg->Arrival;
				$legData['duration'] = $leg->Duration;
				$legData['journeyMode'] = $leg->JourneyMode;
				if(sizeof($leg->Stops) > 0)
					$legData['stopIds'] = implode(",", $leg->Stops);
				else
					$legData['stopIds'] = null;
				$legData['carrierIds']= implode(",", $leg->Carriers);
				$legData['operatingCarrierIds']= implode(",", $leg->OperatingCarriers);
				$flightNumbers = "";
				foreach ($leg->FlightNumbers as $flight) {
					$flightNumbers .= $flight->FlightNumber."(".$flight->CarrierId.")," ;
				}
				$legData['flightNumbers'] = substr($flightNumbers,0,strlen($flightNumbers) -1);
				$legData['directionality']= $leg->Directionality;
				array_push($dataArray, $legData);
			}
			if(sizeof($dataArray) > 0)
				$this->insertIntoQueryLegs($dataArray);
			else
			{
				$msg = "Prices Model - insertIntoQueryLegs - Error in dataArray ==> ".json_encode($dataArray).PHP_EOL;
				$this->logger->logError($msg,"prices");
				throw new Exception("insertIntoQueryLegs - Error in dataArray");
			}

			


			/** 
			  * Functionality to insert in Query Itineraries Master
			  **/
			foreach ($this->decodedResponse->Itineraries as $itinerary) {
				$itineraryMasterData['queryId'] = $queryId;
				$itineraryMasterData['outboundLegId'] = $this->getLegId($queryId,$itinerary->OutboundLegId);
				$itineraryMasterData['inboundLegId'] = $this->getLegId($queryId,$itinerary->InboundLegId);
				$itineraryMasterData['uri'] = $itinerary->BookingDetailsLink->Uri;
				$itineraryMasterData['body'] = $itinerary->BookingDetailsLink->Body;
				$itineraryMasterData['method']= $itinerary->BookingDetailsLink->Method;
				$response = $this->insertIntoQueryItineraryMaster($itineraryMasterData);
				if($response['id'] == null){
					$msg = "Prices Model - Error in inserting in Query Itinerary Master . QueryItinerary Master id : ".$response['id'].PHP_EOL;
					$this->logger->logError($msg,"prices");
				}

				else
					$itineraryId = $response['id'];
				/** 
			 	  * Functionality to insert in Query Itinerary Agents
			  	  **/
				$dataArray = array();
				foreach ($itinerary->PricingOptions as $pricingOption) {
					$itineraryAgentData['itineraryId'] = $itineraryId;
					$itineraryAgentData['agentIds'] = implode(",", $pricingOption->Agents);
					$itineraryAgentData['ageInMinutes'] = $pricingOption->QuoteAgeInMinutes;
					$itineraryAgentData['price'] = $pricingOption->Price;
					$itineraryAgentData['deepLinkUrl'] = $pricingOption->DeeplinkUrl;
					array_push($dataArray, $itineraryAgentData);
				}	
				if(sizeof($dataArray) > 0)
					$this->insertIntoQueryItineraryAgents($dataArray);
				else
				{
					$msg = "Prices Model -insertIntoQueryItineraryAgents - Error in dataArray ==> ".json_encode($dataArray).PHP_EOL;
					$this->logger->logError($msg,"prices");
					throw new Exception("insertIntoQueryItineraryAgents - Error in dataArray");
				}
				
			}

			$error = $this->db->error();
			if($this->db->error() && $error['code']>0)
			{	
				$msg = "Prices Model - Error in parsing Live Responses ======= ErrorCode : ".$error['code']." =========== Error Message : ".$error['message']." ========== Query : ".$this->db->last_query().PHP_EOL;
				$this->logger->logDbError($msg,"prices");

				$this->db->trans_rollback();
				$fdResult = $this->insertFailedQuery($postData);
				if ($fdResult == 1 ) {
					$msg = "Prices Model - Query inserted in  queryFailed: ".json_encode($postData).PHP_EOL;
					$this->logger->logInfo($msg,"prices");
					return -1;
				}
				else {
					$msg = "Prices Model - Error in inserting query in  queryFailed: ".json_encode($postData).PHP_EOL;
					$this->logger->logError($msg,"prices");
					return 0;
				}
			}
			else
			{
				$msg = "Prices Model - Everything has been inserted properly for data ".json_encode($postData).PHP_EOL;
				$this->logger->logInfo($msg,"prices");
				$this->db->trans_commit();
				return 1;
			}
			

		} catch(Exception $ex){
			$msg = "Prices Model - Error while parsing reponse : ".$ex->getMessage();
			$this->logger->logError($msg,"prices");
			$this->db->trans_rollback();
			$fdResult = $this->insertFailedQuery($postData);
			if ($fdResult == 1 ) {
				$msg = "Prices Model - Query inserted in  queryFailed: ".json_encode($postData).PHP_EOL;
				$this->logger->logInfo($msg,"prices");
				return -1;
			}
			else {
				$msg = "Prices Model - Error in inserting query in  queryFailed: ".json_encode($postData).PHP_EOL;
				$this->logger->logError($msg,"prices");
				return 0;
			}
		}
	}

	/** 
	  * Functionality to get Status Code froom StatusMaster
	  **/
	public function getStatusCode($status)
	{
		$this->db->select('code');
		$result = $this->db->get_where($this->statusMaster,array('status' => $status));
		$result = $result->result_array();
		return $result[0]['code'];
	}

	/** 
	  * Functionality to generated New batch ID by reading the last batch Id in DB
	  **/
	public function getNewBatchId()
	{
		$this->db->select('id');
		$this->db->order_by('id','desc');
		$result = $this->db->get($this->batchMaster,0,1);
		$result = $result->result_array();
		if($result['id'] == null)
			return null;
		else
			$oldId = $result['id']; 
		$newId = $oldId++;
		return $newId;
	}

	/** 
	  * Functionality to insert in Branch Master
	  **/
	public function insertIntoBatchMaster($data)
	{
		$this->db->insert($this->batchMaster,$data);
		$id = $this->db->insert_id();
		return $id;
	}

	/** 
	  * Functionality to update the Batch as complete
	  **/
	public function updateBatchMasterAsCompleted()
	{
		$data['isCompleted'] = 1;
		$result = $this->db->update($this->batchMaster,$data);
		return $result;
	}

	/** 
	  * Functionality to insert in Query Master
	  **/
	public function insertIntoQueryMaster($data)
	{
		$result = $this->db->insert($this->queryMaster,$data);
		$response['result'] = $result;
		$response['id'] = $this->db->insert_id();
		return $response;
	}
	/** 
	  * Functionality to batch insert in Query Currencies
	  **/
	public function insertIntoQueryCurrencies($data)
	{
		$result = $this->db->insert_batch($this->queryCurrencies,$data);		
		return $result;
	}

	/** 
	  * Functionality to batch insert in Query Places
	  **/
	public function insertIntoQueryPlaces($data)
	{
		$result = $this->db->insert_batch($this->queryPlaces,$data);		
		return $result;
	}

	/** 
	  * Functionality to batch insert in Query Agents
	  **/
	public function insertIntoQueryAgents($data)
	{
		$result = $this->db->insert_batch($this->queryAgents,$data);		
		return $result;
	}

	/** 
	  * Functionality to batch insert in Query Carriers
	  **/
	public function insertIntoQueryCarriers($data)
	{
		$result = $this->db->insert_batch($this->queryCarriers,$data);		
		return $result;
	}

	/** 
	  * Functionality to batch insert in Query Segments
	  **/
	public function insertIntoQuerySegments($data)
	{
		$result = $this->db->insert_batch($this->querySegments,$data);		
		return $result;
	}

	/** 
	  * Functionality to batch insert in Query Legs
	  **/
	public function insertIntoQueryLegs($data)
	{

		//$result = $this->db->insert($this->queryLegs,$data);
		$result = $this->db->insert_batch($this->queryLegs,$data);		
		return $result;
	}

	/** 
	  * Functionality to batch insert in Query Itinerary Master
	  **/
	public function insertIntoQueryItineraryMaster($data)
	{

		$result = $this->db->insert($this->queryItineraryMaster,$data);
		$response['result'] = $result;
		$response['id'] = $this->db->insert_id();		
		return $response;
	}

	/** 
	  * Functionality to get the PK(id) of the corresponding LegID form query legs table
	  **/
	public function getLegId($queryId,$legId){
		$this->db->select['id'];
		$result = $this->db->get_where($this->queryLegs,array('queryId' => $queryId, 'legId'=>$legId));
		$result = $result->result_array();
		return $result[0]['id'];
	}

	/** 
	  * Functionality to batch insert in Query Agents
	  **/
	public function insertIntoQueryItineraryAgents($data)
	{

		$result = $this->db->insert_batch($this->queryItineraryAgents,$data);		
		return $result;
	}
	
	/** 
	  * Functionality to convert data array into post form
	  **/
	public function convertToForm($data){
		foreach ($data as $key => $value) {
			$x .= urlencode($key)."=".urlencode($value)."&";
		}
		$x = substr($x, 0, strlen($x)-1);
		return $x;
	}

	public function getAirportId($airportCode)
	{
		$this->db->select('id');
		$result = $this->db->get_where($this->airportMaster,array('airportCode'=>$airportCode));
		if ($result != null) { 
			$result = $result->result_array();
			if ($result[0]['id'] != null) 
				return $result[0]['id'];
		}
		else
			return 0 ;	
	}
	/** 
	  * Functionality to insert failed query in table
	  **/
	public function insertFailedQuery($postData){
		$data['outboundDate'] = $postData['outbounddate'];
		$data['inboundDate'] = $postData['inbounddate'];
		$data['sourceAirportId'] = $this->getAirportId($postData['originplace']);
		$data['destinationAirportId'] =  $this->getAirportId($postData['destinationplace']);
		if($data['outboundDate'] == 0 || $data['inboundDate'] == 0)
			return 0;
		$result = $this->db->insert($this->queryFailed,$data);
		return $result;
	}

	public function updateFailedQueryAsCompleted($data){

	}

}
