<?php
/*error_reporting(E_ALL);
ini_set('display_errors',1);
error_reporting(0);*/

class Route_model extends CI_Model
{
    public $string;

    public function __construct(){
		$ci =& get_instance();
    	$this->load->database('default');
		$this->routeMaster = "routeMaster";
		$this->airportMaster = "airportMaster";
    }
	
    /** @AK-13/03/2017
	  * 
	  * Get all routes from routeMaster Table
	  * 
	  * @params (null)

	  * @returns([array] of all routes in the routeMaster table)
	  **/
	public function getAllRoutes()
	{
		$this->db->select('rm.*,ams.airportName as fromAirportName,ams.airportCode as fromAirportCode,amd.airportName as toAirportName,amd.airportCode as toAirportCode');
		$this->db->order_by('id','desc');
		$this->db->join($this->airportMaster." as ams","ams.id = rm.fromId");
		$this->db->join($this->airportMaster." as amd","amd.id = rm.toId");
		$result = $this->db->get($this->routeMaster." as rm");
		return $result->result_array();
	}

	/** @Akash-13/03/2017
	  * 
	  * Adds an airport to the Route Master Table
	  * 
	  * @params (Data array having keys Id of source Airport and Id of Dstination Airport)
	  * @returns(1 if success, 0 if failed)
	  *	  
	  **/
	public function addRoute($data)
	{
		$result = $this->db->insert($this->routeMaster,$data);
		return $result;
	}


    /** @AK-13/03/2017
	  * 
	  * Remove an airport from the routeMaster and delete all its entries from Airport Master
	  * 
	  * @params (cityId , airportId and airportCode of the airport to be deleted)
	  * @returns(0 or 1)
	  **/
	public function removeRoute($data)
	{
		$result = $this->db->delete($this->routeMaster,array('id' => $data['id']));
		return $result;
	}



}
