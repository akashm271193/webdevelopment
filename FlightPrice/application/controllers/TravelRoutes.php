<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);

class TravelRoutes extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('route_model',"routeModel");
		$this->load->model('airport_model',"airportModel");
		$this->load->library('logging_lib',null,"logger");
		$this->className = $this->router->class;	
	}

	public function index() 
	{
		$data['routeMaster'] = $this->routeModel->getAllRoutes();
		$data['routeCount'] = sizeof($data['routeMaster']);
		$this->load->view('travelRoutes', $data);
	}

	/** @AK-13/03/2017
	  * 
	  * Function to open the page to Add route
	  * 
	  * @params (NULL)
	  * @returns(Loads the add route page with a list of all cities)
	  **/
	function openAddRoute()
	{
		$data['airportMaster'] = $this->airportModel->getAllAirports();
		$data['airportCount'] = sizeof($data['airportMaster']);
		$this->load->view('addTravelRoute', $data);
	}


	/** @AK-13/03/2017
	  * 
	  * Function to add a route
	  * 
	  * @params (Post call containing source Airport Id and Destination Airport Id)
	  * @returns(Success or failure message)
	  **/
	function addRoute()
	{
		$data['fromId'] = $this->input->post('fromAirportId');
		$data['toId'] = $this->input->post('toAirportId');
		$msg ="Routes Controller - Request for adding a   with params : ".json_encode($data).PHP_EOL;		
		$this->logger->logInfo($msg,$this->className);
		$result = $this->routeModel->addRoute($data);
		if($result == 1){
			$msg ="Routes Controller - Your route has been added Successfully with params : ".json_encode($data).PHP_EOL;
			$this->logger->logInfo($msg,$this->className);
			redirect('travelRoutes');
		}
		else {
			$msg ="Routes Controller - Oops ... We encountered an error while adding the route. Please try again later!".json_encode($data).PHP_EOL;
			$this->logger->logError($msg,$this->className);
			redirect('travelRoutes/openAddRoute');
		}
	}

	
	/** @AK-13/03/2017
	  * 
	  * Function to remove a route
	  * 
	  * @params (Post call containing routeId , cityId and routeCode)
	  * @returns(Success or failure message)
	  **/
	function removeRoute()
	{
		$data['id'] = $this->input->post('id');
		$msg ="Routes Controller - Request for removing a route with params : ".json_encode($data).PHP_EOL;		
		$this->logger->logInfo($msg,$this->className);
		$result = $this->routeModel->removeRoute($data);
		if($result == 1){
			$msg ="Routes Controller - Successfully deleted route with params : ".json_encode($data).PHP_EOL;
			$this->logger->logInfo($msg,$this->className);
		}
		else {
			$msg ="Routes Controller - Error in deleting route with params : ".json_encode($data).PHP_EOL;
			$this->logger->logError($msg,$this->className);
		}
		redirect('travelRoutes');
	}
}
