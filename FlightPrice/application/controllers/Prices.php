<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*error_reporting(E_ALL);
ini_set('display_errors',1);*/
error_reporting(0);
set_time_limit(0);
class Prices extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->model('price_model','priceModel');
		$this->load->model('route_model','routeModel');
		$this->load->library('parallel_process_lib',null,"ppLib");
		$this->load->library('logging_lib',null,"logger");
		$this->className = $this->router->class;	

		//$this->load->library('parallelprocesslib');
		//$this->load->library('ssapi_lib');
		//$this->priceModel = $this->load->model('flightLivePricesModel');
	}

	public function index() 
	{
		
	}



	public function processMonth($monthFromNow)
	{
		$months = array("00","01","02","03","04","05","06","07","08","09","10","11","12");
		if(!isset($monthFromNow) || !in_array($monthFromNow, $months))
		{
			$msg = "Prices Controller - Invalid month passed : ".$monthFromNow.PHP_EOL;
			$this->logger->logError($msg,$this->className);
			die;
		}

		//$url = base_url()."index.php/flightLivePrices/startQueryForMonth/".$monthFromNow;
		try {
			$url = base_url()."index.php/prices/startQueryForMonth/00";
			$this->ppLib->doInBackground($url);	
			$url = base_url()."index.php/prices/startQueryForMonth/01";
			$this->ppLib->doInBackground($url);	
		} catch (Exception $e) {
			$msg = "Prices Controller - Error while forking process for monthFromNow : ".$monthFromNow.PHP_EOL;
			$this->logger->logError($msg,$this->className);
		}
		

	}

	public function startQueryForMonth($monthFromNow){
		$start = microtime(true);
		$msg = "Prices Controller - Starting script for monthFromNow : ".$monthFromNow." ::: script start time : ".$start.PHP_EOL;
		$this->logger->logInfo($msg,$this->className);
		$monthFromNow = (int) $monthFromNow;
		$currentMonth = date('Y-m',time());
		$desiredMonth = date('Y-m', strtotime("+".$monthFromNow." months"));
		$msg = "Prices Controller - Month Desired : ".$monthFromNow."  ============ Month being parsed : ".$desiredMonth.PHP_EOL;
		$this->logger->logInfo($msg,$this->className);
		if($desiredMonth == $currentMonth)
			$start_date = date('Y-m-d',time());
		else 
			$start_date = $desiredMonth."-01";

		//$start_date = "2018-02-01";
		$start_time = strtotime($start_date);
		$nextMonth = date ("m-Y",strtotime("+1 month", strtotime($desiredMonth."-01")));
		//echo $nextMonth."<br>";
		$firstDayNextMonth = "01-".$nextMonth;
		$end_time = strtotime($firstDayNextMonth);
		$msg = "Prices Controller - Days selected in the month - Start_date : ".$start_date."  ============ End date : ".date("Y-m-d", strtotime("-1 day",$end_time)).PHP_EOL;
		$this->logger->logInfo($msg,$this->className);
		for($i=$start_time; $i<$end_time; $i+=86400)
		{
		   $startDateList[] = date('Y-m-d', $i);
		}
		//print_r($startDateList);

		$endDateList = $startDateList;
		$sdlSize = sizeof($startDateList);
		$possibleDates = array();
		for($i=0;$i<$sdlSize;$i++)
		{
			if(sizeof($endDateList)-1-$i >= 15)
			{
				for($j=$i;$j<$i+15;$j++)
				{
					$data = array();
					$data['start_date'] = $startDateList[$i];
					$data['end_date'] = $endDateList[$j];
					array_push($possibleDates, $data);
				}
			}
			else
			{
				$start_date = $startDateList[$sdlSize-1];
				$start_time = strtotime($start_date);
				$extraDate = date ("Y-m-d",strtotime("+15 days", $start_time));
				$end_time = strtotime($extraDate);
				for($k=$start_time+86400; $k<$end_time; $k+=86400)
				{
				   array_push($endDateList, date('Y-m-d', $k));
				}
				$i--;
			}
		}

		$this->buildQuery($possibleDates,$start);
	}

	

	public function buildQuery($possibleDates,$start){
		$cityList = $this->routeModel->getAllRoutes();
		if(isset($cityList) && $cityList != null && sizeof($cityList) > 0)
		{
			foreach ($possibleDates as $data) {
				foreach ($cityList as $city) {
				 	$data['fromAirportCode'] = $city['fromAirportCode'];
				 	$data['toAirportCode'] = $city['toAirportCode'];
				 	$this->getLivePrices($data);
				 } 
			}
			$time_elapsed_secs = microtime(true) - $start;
			$msg = "Prices Controller - Ending script for monthFromNow : ".$monthFromNow." :::: time lapsed : ".$time_elapsed_secs.PHP_EOL;
			$this->logger->logInfo($msg,$this->className);
	
		}
		else{	
			$msg = "Prices Controller - No routes fetched. Exitting the script ".PHP_EOL;
			$this->logger->logError($msg,$this->className);
			die;
		}

	}


	/** @AK-13/03/2017
	  * 
	  * Function to open the page to Add airport
	  * 
	  * @params (NULL)
	  * @returns(Loads the add airport page with a list of all cities)
	  **/
	function getLivePrices($data)
	{
		$data['cabinclass'] = "Economy";
		$data['country'] = "UK";
		$data['currency'] = "GBP";
		$data['locale'] = "en-GB";
		$data['locationSchema'] = "iata";
		$data['originplace'] = $data['fromAirportCode'];
		$data['destinationplace'] = $data['toAirportCode'];
		$data['outbounddate'] = $data['start_date'];
		$data['inbounddate'] = $data['end_date'];
		$data['adults'] = "1";
		$data['children'] = "0"; 
		$data['infants'] = "0";
		$data['apikey'] = $this->config->item('apiKey');
		print_r($data);
		echo "<br>"; 
		$result = $this->priceModel->getLivePrices($data);
		if($result == 1)
			echo('Live Prices Data successfully fetched <br>');
		else if($result == -1)
			echo('Query inserted in Query Failed <br>');
		else
			echo('Error while fetching live Prices <br>');
		/*$data['cityMaster'] = $this->cityModel->getAllCities();
		$data['cityCount'] = sizeof($data['cityMaster']);
		$this->load->view('addAirport', $data);*/
	}

}
