<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
class Airport extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('airport_model',"airportModel");
		$this->load->model('city_model','cityModel');
		$this->load->library('logging_lib',null,"logger");
		$this->className = $this->router->class;
	
	}

	public function index() 
	{
		$data['airportMaster'] = $this->airportModel->getAllAirports();
		$data['airportCount'] = sizeof($data['airportMaster']);
		$this->load->view('airport', $data);
	}


	/** @AK-13/03/2017
	  * 
	  * Function to open the page to Add airport
	  * 
	  * @params (NULL)
	  * @returns(Loads the add airport page with a list of all cities)
	  **/
	function openAddAirport()
	{
		$data['cityMaster'] = $this->cityModel->getAllCities();
		$data['cityCount'] = sizeof($data['cityMaster']);
		$this->load->view('addAirport', $data);
	}

	/** @AK-13/03/2017
	  * 
	  * Function to add a airport
	  * 
	  * @params (Post call containing airportName and airportCode)
	  * @returns(Success or failure message)
	  **/
	function addAirport()
	{
		$data['cityId'] = $this->input->post('cityId');
		$data['airportName'] = $this->input->post('airportName');
		$data['airportCode'] = $this->input->post('airportCode');
		$data['updateTime'] = 'NOW()';
		$msg = "Airport Controller - Request for adding a airport  with params : ".json_encode($data).PHP_EOL;		
		$this->logger->logInfo($msg,$this->className);
		$result = $this->airportModel->addAirport($data);
		if($result == 1){
			$msg = "Airport Controller - Your airport has been added Successfully with params : ".json_encode($data).PHP_EOL;
			$this->logger->logInfo($msg,$this->className);
			redirect('airport');
		}
		else {
			$msg = "Airport Controller - Oops ... We encountered an error while adding the airport. Please try again later!".json_encode($data).PHP_EOL;
			$this->logger->logError($msg,$this->className);
			redirect('airport/openAddAirport');
		}
	}

	
	/** @AK-13/03/2017
	  * 
	  * Function to remove a airport
	  * 
	  * @params (Post call containing airportId , cityId and airportCode)
	  * @returns(redirects to airpport listing page)
	  **/
	function removeAirport()
	{
		$data['id'] = $this->input->post('id');
		$data['cityId'] = $this->input->post('cityId');
		$data['airportCode'] = $this->input->post('airportCode');
		$msg = "Airport Controller - Request for removing a airport with params : ".json_encode($data).PHP_EOL;		
		$this->logger->logInfo($msg,$this->className);
		$result = $this->airportModel->removeAirport($data);
		if($result == 1){
			$msg = "Airport Controller - Successfully deleted airport with params : ".json_encode($data).PHP_EOL;
			$this->logger->logInfo($msg,$this->className);
		}
		else {
			$msg ="Airport Controller - Error in deleting airport with params : ".json_encode($data).PHP_EOL;
			$this->logger->logError($msg,$this->className);
		}
		redirect('airport');
	}
}
