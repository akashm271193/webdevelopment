<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);

class Home extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->helper('form');
		$this->className = $this->router->class;	
	}

	public function index() 
	{
		$this->load->view('home');
	}
}
