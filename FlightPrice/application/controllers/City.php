<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*error_reporting(E_ALL);
ini_set('display_errors',1);*/
error_reporting(0);

class City extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('city_model',"cityModel");	
		$this->load->library('logging_lib',null,"logger");
		$this->className = $this->router->class;
	}

	public function index()
	{
		$data['cityMaster'] = $this->cityModel->getAllCities();
		$data['cityCount'] = sizeof($data['cityMaster']);
		$this->load->view('city',$data);
		
	}

	/** @AK-18/01/2016
	  * 
	  * Function to open add city page
	  * 
	  * @params (Null)
	  * @returns(Loads the view of add city page)
	  **/
	function openAddCity()
	{
		$this->load->view('addCity');	
	}

	/** @AK-18/01/2016
	  * 
	  * Function to add a city
	  * 
	  * @params (Post call containing cityName and cityCode)
	  * @returns(Success or failure message)
	  **/
	function addCity()
	{
		$data['cityName'] = $this->input->post('cityName');
		$data['cityCode'] = $this->input->post('cityCode');
		$msg = "City Controller - Request for adding a city  with params : ".json_encode($data).PHP_EOL;
		echo "City : ".$this->className."<br/>";	
		$this->logger->logInfo($msg,$this->className);
		$result = $this->cityModel->addCity($data);
		if($result == 1){
			$msg = "City Controller - Your city has been added Successfully with params : ".json_encode($data).PHP_EOL;
			$this->logger->logInfo($msg,$this->className);
			redirect('city');
		}
		else {
			$msg = "City Controller - Oops ... We encountered an error while adding the city. Please try again later!".json_encode($data).PHP_EOL;
			$this->logger->logError($msg,$this->className);
			redirect('city/openAddCity');
		}
	}

	
		/** @AK-18/01/2016
	  * 
	  * Function to remove a city
	  * 
	  * @params (Post call containing cityId and cityCode)
	  * @returns(Success or failure message)
	  **/
	function removeCity()
	{
		$data['id'] = $this->input->post('id');
		$data['cityCode'] = $this->input->post('cityCode');
		$msg = "City Controller - Request for removing a city with params : ".json_encode($data).PHP_EOL;		
		$this->logger->logInfo($msg,$this->className);
		$result = $this->cityModel->removeCity($data);
		if($result == 1){
			$msg = "City Controller - Successfully deleted city with params : ".json_encode($data).PHP_EOL;
			$this->logger->logInfo($msg,$this->className);
		}
		else {
			$msg = "City Controller - Error in deleting city with params : ".json_encode($data).PHP_EOL;
			$this->logger->logError($msg,$this->className);
		}
		redirect('city');
	}
}
