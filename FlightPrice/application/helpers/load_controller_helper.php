<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('load_api'))
{
    function load_api($controller, $method = 'index', $param1)
    {
        require_once(APPPATH . 'controllers/apis/' . $controller . '.php');

        $controller = new $controller();

        if(isset($param1) && $param1 != "")
        	return $controller->$method($param1);
        else
        	return $controller->$method();
    }
}

if (!function_exists('load_controller'))
{
    function load_controller($controller, $method = 'index', $param1)
    {
        require_once(APPPATH . 'controllers/' . $controller . '.php');

        $controller = new $controller();

        if(isset($param1) && $param1 != "")
        	return $controller->$method($param1);
        else
        	return $controller->$method();
    }
}

?>