CREATE DATABASE flightPrices;

USE flightPrices;

CREATE TABLE `cityMaster` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique Id for each city',
  `cityName` varchar(50) NOT NULL COMMENT 'Name of the city',
  `cityCode` varchar(30) NOT NULL COMMENT 'City code for API',
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Creation Time of the City',
  `updateTime` timestamp NULL COMMENT 'Update Time of the City',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cityCode` (`cityCode`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

CREATE TABLE `airportMaster` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique Id for each city',
  `cityId` int(11) NOT NULL COMMENT 'Name of the city',
  `airportName` varchar(50) NOT NULL COMMENT 'Name of the Airports',
  `airportCode` varchar(10) NOT NULL COMMENT 'Airports code for API',
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Creation Time of the City',
  `updateTime` timestamp NULL COMMENT 'Update Time of the City',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UniqueAirportCity` (`cityId`,`airportCode`),
  FOREIGN KEY FK_CITY_ID(cityId)
  REFERENCES cityMaster(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

CREATE TABLE `routeMaster` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique Id for each route',
  `fromId` int(11) NOT NULL COMMENT 'Id of the source Airport',
  `toId` int(11) NOT NULL COMMENT 'Id of the destination Airport',
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Creation Time of the Route',
  `updateTime` timestamp NULL COMMENT 'Update Time of the route',
  PRIMARY KEY (`id`),
  FOREIGN KEY FK_ROUTE_FROM_AIRPORT_ID (fromId)
  REFERENCES airportMaster(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE,
  FOREIGN KEY FK_ROUTE_TO_AIRPORT_ID (toId)
  REFERENCES airportMaster(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

CREATE TABLE `batchMaster` (
 `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique Id for each batch executed',
 `isCompleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Indicator if the batch is completed or not',
 `executionTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Execution tIme of the batch',
 `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Entry Creation',
 `updateTime` timestamp NULL COMMENT 'Update time of the Entry',
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

CREATE TABLE `batchToExecutionMapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique Id for each entry',
  `batchId` int(11) NOT NULL COMMENT 'Id of the the batch executed',
  `monthExecuted` VARCHAR(5) NOT NULL COMMENT 'Month executed in the batch',
  `executionTime` datetime NOT NULL COMMENT 'Execution tIme of the batch',
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Entry Creation',
  `updateTime` timestamp NULL COMMENT 'Update time of the Entry',
  PRIMARY KEY (`id`),
  FOREIGN KEY FK_EXECUTION_BATCHID(batchId)
  REFERENCES batchMaster(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

CREATE TABLE `statusMaster` (
  `code` VARCHAR(5) NOT NULL COMMENT 'Unique code for each status',
  `status` VARCHAR(50) NOT NULL COMMENT 'Actual Status',
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Creation time of the status',
  `updateTime` timestamp NULL COMMENT 'Update time of the status',
  PRIMARY KEY (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

CREATE TABLE `queryMaster` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique Id for each entry',
  `batchId` int(11) NOT NULL COMMENT 'Id of the the batch executed',
  `sessionKey` VARCHAR(150) NOT NULL COMMENT 'Session of the query',
  `query` VARCHAR(600) NOT NULL COMMENT 'Query sent across', 
  `status` VARCHAR(5) NOT NULL COMMENT 'status of the query',
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Entry Creation',
  `updateTime` timestamp NULL COMMENT 'Update time of the Entry',
  PRIMARY KEY (`id`),
  FOREIGN KEY FK_QUERY_BATCH_ID(batchId)
  REFERENCES batchMaster(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE,
  FOREIGN KEY FK_QUERY_BATCH_STATUS(status)
  REFERENCES statusMaster(code)
  ON UPDATE CASCADE
  ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;


CREATE TABLE `queryCurrencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique Id for each entry',
  `queryId` int(11) NOT NULL COMMENT 'Id of the the batch executed',
  `currencyCode` VARCHAR(10) NOT NULL COMMENT 'Code of the currency',
  `currencySymbol` VARCHAR(3) NOT NULL COMMENT 'Symbol of the currency',
  `thousandsSeparator` VARCHAR(3) NOT NULL COMMENT 'Spearator of thousands',
  `decimalSeparator` VARCHAR(3) NOT NULL COMMENT 'Separator of decimal',
  `symbolOnLeft` TINYINT(1) NOT NULL DEFAULT 1 COMMENT 'symbol on left or not',
  `spaceBetweenAmountAndSymbol`  TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'Space between symbol and amount',
  `roundingCoefficient` int(3) NOT NULL COMMENT 'rounding coefficient',
  `decimalDigits` int(2) NOT NULL COMMENT 'No, of digits to be considered after decimal',
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Entry Creation',
  `updateTime` timestamp NULL COMMENT 'Update time of the Entry',
  PRIMARY KEY (`id`),
  FOREIGN KEY FK_CURRENCY_QUERY_ID(queryId)
  REFERENCES queryMaster(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;



CREATE TABLE `queryPlaces` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique Id for each entry',
  `queryId` int(11) NOT NULL COMMENT 'Id of the the batch executed',
  `placeId` int(11) NOT NULL COMMENT 'Id of the place',
  `placeParentId` int(10) NOT NULL COMMENT 'Id of the parent place',
  `placeCode` VARCHAR(5) NOT NULL COMMENT 'Code of the place',
  `placeType` VARCHAR(10) NOT NULL COMMENT 'Type of place',
  `placeName` VARCHAR(20) NOT NULL COMMENT 'Name of the place',
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Entry Creation',
  `updateTime` timestamp NULL COMMENT 'Update time of the Entry',
  PRIMARY KEY (`id`),
  FOREIGN KEY FK_PLACE_QUERY_ID(queryId)
  REFERENCES queryMaster(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;


CREATE TABLE `queryAgents` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique Id for each entry',
  `queryId` int(11) NOT NULL COMMENT 'Id of the the batch executed',
  `agentId` int(11) NOT NULL COMMENT 'Id of the agent',
  `agentName` VARCHAR(30) NOT NULL COMMENT 'Name of the agent',
  `agentImage` VARCHAR(100) NOT NULL COMMENT 'Image for the agent',
  `agentStatus` VARCHAR(5) NOT NULL COMMENT 'Status of the agent',
  `agentOptimisedForMobile` TINYINT(1) NOT NULL DEFAULT 1 COMMENT 'Is optimized for mobile',
  `agentBookingNumber` VARCHAR(20) NULL COMMENT 'Booking number of the agent',
  `agentType` VARCHAR(20) NOT NULL COMMENT 'Type of the agent',
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Entry Creation',
  `updateTime` timestamp NULL COMMENT 'Update time of the Entry',
  PRIMARY KEY (`id`),
  FOREIGN KEY FK_AGENTS_QUERY_ID(queryId)
  REFERENCES queryMaster(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE,
  FOREIGN KEY FK_AGENTS_STATUS_CODE(agentStatus)
  REFERENCES statusMaster(code)
  ON UPDATE CASCADE
  ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

CREATE TABLE `queryCarriers` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique Id for each entry',
  `queryId` int(11) NOT NULL COMMENT 'Id of the the query',
  `carrierId` int(11) NOT NULL COMMENT 'Id of the carrier',
  `carrierCode` VARCHAR(10) NOT NULL COMMENT 'Code of the carrier',
  `carrierName` VARCHAR(30) NOT NULL COMMENT 'Name of the carrier',
  `carrierImage` VARCHAR(100) NOT NULL COMMENT 'Image of the carrier',
  `carrierDisplayCode` VARCHAR(10) NOT NULL COMMENT 'code of the carrier to be displayed',
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Entry Creation',
  `updateTime` timestamp NULL COMMENT 'Update time of the Entry',
  PRIMARY KEY (`id`),
  FOREIGN KEY FK_CARRIER_QUERY_ID(queryId)
  REFERENCES queryMaster(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

CREATE TABLE `querySegments` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique Id for each segment',
  `queryId` int(11) NOT NULL COMMENT 'Id of the the query',
  `segmentId` int(11) NOT NULL COMMENT 'Id of the segment',
  `originStation` int(10) NOT NULL COMMENT 'Origin station of the segment',
  `destinationStation` int(10) NOT NULL COMMENT 'Destination station of the segment',
  `departureTime` datetime NOT NULL COMMENT 'Departure time of the segement',
  `arrivalTime` datetime NOT NULL COMMENT 'Arrival time of the segment',
  `carrierId` int(11) NOT NULL COMMENT 'Carrier Id of the segment',
  `operatingCarrierId` int(11) NOT NULL COMMENT 'operating carrier Id',
  `duration` int(5) NOT NULL COMMENT 'duration of the segment',
  `flightNumber` int(11) NOT NULL COMMENT 'number of the carrier',
  `journeyMode` VARCHAR(20) NOT NULL COMMENT 'Journey mode of the segment',
  `directionality` VARCHAR(20) NOT NULL COMMENT 'directionality of the segment',
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Entry Creation',
  `updateTime` timestamp NULL COMMENT 'Update time of the Entry',
  PRIMARY KEY (`id`),
  FOREIGN KEY FK_SEGMENT_QUERY_ID(queryId)
  REFERENCES queryMaster(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;


CREATE TABLE `queryLegs` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique Id for each leg',
  `queryId` int(11) NOT NULL COMMENT 'Id of the the query',
  `legId` VARCHAR(100) NOT NULL COMMENT 'Id of the leg',
  `segmentIds` VARCHAR(20) NOT NULL COMMENT 'Segment Ids',
  `originStation` int(10) NOT NULL COMMENT 'Id of the start point',
  `destinationStation` int(10) NOT NULL COMMENT 'Id of the end point',
  `departureTime` datetime NOT NULL COMMENT 'Departure time of leg',
  `arrivalTime` datetime NOT NULL COMMENT 'arrival time of the leg',
  `duration` int(5) NOT NULL COMMENT 'duration of the leg',
  `journeyMode` VARCHAR(20) NOT NULL COMMENT 'journey Mode of the leg',
  `stopIds` VARCHAR(20) NULL COMMENT 'stop ids in the leg',
  `carrierIds` VARCHAR(30) NOT NULL COMMENT 'carriers ids in the leg',
  `operatingCarrierIds` VARCHAR(30) NOT NULL COMMENT 'carrier ids in the leg',
  `flightNumbers` VARCHAR(50) NOT NULL COMMENT 'flight nummbers in the leg',
  `directionality` VARCHAR(20) NULL COMMENT 'Directionality of the leg',
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Leg Creation',
  `updateTime` timestamp NULL COMMENT 'Update time of the leg',
  PRIMARY KEY (`id`),
  FOREIGN KEY FK_LEGS_QUERY_ID(queryId)
  REFERENCES queryMaster(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

CREATE TABLE `queryItineraryMaster` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique Id for each itinerary',
  `queryId` int(11) NOT NULL COMMENT 'Id of the the query',
  `outboundLegId` int(11) NOT NULL COMMENT 'Id (PK) of the legs table',
  `inboundLegId` int(11) NOT NULL COMMENT 'Id (PK) of the legs table',
  `uri` VARCHAR(100) NOT NULL COMMENT 'Uri of the Itinerary',
  `body`  VARCHAR(100) NOT NULL COMMENT 'Body of the Itinerary',
  `method`  VARCHAR(10) NOT NULL COMMENT 'Method of the Itinerary',
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Entry time of the Itenary',
  `updateTime` timestamp NULL COMMENT 'Update time of the Itenary',
  PRIMARY KEY (`id`),
  FOREIGN KEY FK_ITINERARY_MASTER_QUERY_ID(queryId)
  REFERENCES queryMaster(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE,
  FOREIGN KEY FK_CARRIER_OUTBOUND_ID(outboundLegId)
  REFERENCES queryLegs(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE,
  FOREIGN KEY FK_CARRIER_INBOUND_ID(inboundLegId)
  REFERENCES queryLegs(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;


CREATE TABLE `queryItineraryAgents` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique Id for each itinerary',
  `itineraryId` int(11) NOT NULL COMMENT 'Id (PK) of the Itinerary Master table',
  `agentIds` VARCHAR(50) NOT NULL COMMENT 'Comma separated ids of the agents',
  `ageInMinutes` int(10) NOT NULL COMMENT 'total time',
  `price` DECIMAL(14,2) NOT NULL COMMENT 'Price of the agent',
  `deepLinkUrl` VARCHAR(1300) NOT NULL COMMENT 'Deeplink url',
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Entry time of the Itenary Agent',
  `updateTime` timestamp NULL COMMENT 'Update time of the Agent',
  PRIMARY KEY (`id`),
  FOREIGN KEY FK_ITINERARY_AGENTS_MASTER_ID(itineraryId)
  REFERENCES queryItineraryMaster(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;


CREATE TABLE `queryFailed` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique Id for each entry',
  `outboundDate` date NOT NULL COMMENT "Query start date",
  `inboundDate` date NOT NULL COMMENT "Query end date",
  `sourceAirportId` int(11) NOT NULL COMMENT "Query source airport ID",
  `destinationAirportId`int(11) NOT NULL COMMENT "Query destination airport ID",
  `isCompleted` TINYINT(1) NOT NULL DEFAULT 0 COMMENT "Indicator is the failed query has been requeried or not",
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Entry Creation',
  `updateTime` timestamp NULL COMMENT 'Update time of the Entry',
  PRIMARY KEY (`id`),
  FOREIGN KEY FK_FAILED_QUERY_SOURCE_AIRPORT_ID(sourceAirportId)
  REFERENCES airportMaster(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE,
  FOREIGN KEY FK_FAILED_QUERY_DESTINATION_AIRPORT_ID(destinationAirportId)
  REFERENCES airportMaster(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;