<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg .tg-yw4l{vertical-align:top}
</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<html>
	<head>
		<title>Email Validation Portal</title>
		<link rel="stylesheet" type="text/css" href="
			<?php echo base_url(); ?>/assets/css/style.css">
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<div id="main">
			<div id="home">
				<?php if(isset($error)){ print_r($error); echo "<br/>";?>
					<button type="submit" onClick = "reloadHome()">Go back to Home page</button>
				<?php } else if(isset($fileLinks)){?>
				<h1> Your transasction Id is : <?php echo $transactionId ; ?>. Please save it for future reference. </h1>
					<table class="tg">
					  <tr>
						<th class="tg-yw4l">File Type</th>
						<th class="tg-yw4l">Link To Download</th>
					  </tr>
					  <tr>
						<td class="tg-yw4l">Valid Files</td>
						<td class="tg-yw4l"><a href="<?php echo $fileLinks['validData']?>"><?php echo $fileLinks['validData']?></a></td>
					  </tr>
					  <tr>
						<td class="tg-yw4l">Did You Mean ? </td>
						<td class="tg-yw4l"><a href="<?php echo $fileLinks['correctedData']?>"><?php echo $fileLinks['correctedData']?></a></td>
					  </tr>
					  <tr>
						<td class="tg-yw4l">Disposable Emails</td>
						<td class="tg-yw4l"><a href="<?php echo $fileLinks['disposableData']?>"><?php echo $fileLinks['disposableData']?></a></td>
					  </tr>
					  <tr>
						<td class="tg-yw4l">Role Based Emails</td>
						<td class="tg-yw4l"><a href="<?php echo $fileLinks['roleBasedData']?>"><?php echo $fileLinks['roleBasedData']?></a></td>
					  </tr>
					  <tr>
						<td class="tg-yw4l">Mailbox Verification Failed Emails</td>
						<td class="tg-yw4l"><a href="<?php echo $fileLinks['mailboxVerificationFailed']?>"><?php echo $fileLinks['mailboxVerificationFailed']?></a></td>
					  </tr>
					</table>
					<button type="submit" onClick = "reloadHome()">Load another form</button>
				<?php } else { ?>
					<h1 style="text-align:center">Welcome To Email Validation Portal</h1>
					<?php //echo form_open_multipart('home/upload'); ?>				
					<form method="post" enctype="multipart/form-data" id="uploadForm">
					First Name : <input type="text" name="firstName" id="firstName" placeholder="Enter your first name"/>
					<br/>
					<br/>
					<br/>
					Last Name : <input type="text" name="lastName" id="lastName" placeholder="Enter your last name"/>
					<br/>
					<br/>
					<br/>
					Csv File  :  <input type="file" name="csvFile" id="csvFile"/>
					<br/>
					<br/>
					<br/>
					<input type="submit" value=" Upload " name="upload" onClick = "verifyUpload()"/>
					</form>
					<button type="submit" onclick="download()">Download Sample File</button>
					<br/>
					<br/>
					<form method = "post" id="historyForm">	
						TransactionID : <input id="transactionId" name="transactionId"/>
						<button onClick = "verify()">Get Details </button>
					</form>
				<?php } ?>
			</div>
		</div>
	</body>
</html>
<script>
function download() {
	var stringUrl = "home/download";	
	
	$.post(stringUrl, "" , function (data) {
		//var regex = new RegExp("^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?");		
		//regex.test(data)
		if ( data != null ) {
			document.location = data;
		} else {
			alert("Error downloading sample file. Please try after sometime.");
		}
	});
}

function verifyUpload() {
	var firstName = $("#firstName").val();
	var lastName = $("#lastName").val();
	if( firstName == "" || firstName == null){
		alert("Invalid First Name !");
	}
	else if( lastName == "" || lastName == null){
		alert("Invalid Last Name !");
	}
	else{
		$("#uploadForm").attr("action","home/upload");
		$("#uploadForm").submit();
	}
}

function verify() {
	var stringUrl = "home/getTransactionHistory";	
	var transactionId = $("#transactionId").val();
	if( transactionId != "" && transactionId != null){
		$("#historyForm").attr("action","home/getTransactionHistory");
		$("#historyForm").submit();
	}
	else{
		alert("Invalid Transaction ID !");
	}
}

function reloadHome() {
	window.location.replace("home"); 
}
</script>