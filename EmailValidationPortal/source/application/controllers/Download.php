<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);

class Download extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('download');
    }

	/** 
	  * Index function, which takes the 2nd parameter from the url , i.e. the file name
	  * 
	  * @params(null)
	  * @return(null)
	  **/
	public function index(){
		$fileName = $this->uri->segment(2);
		if($fileName == "sample.csv"){
			$filePath = $this->config->item("samplesFilePath");
		}
		else{
			$filePath = $this->config->item("endFilesPath");
		}
		$this->downloadFile($fileName,$filePath);
	}
	
	/** 
	  * Function to force download a file 
	  * 
	  * @params ($fileName ----- Name of the file).
	  * @returns(downloads file if successful , echos message if failed)
	  **/
	function downloadFile($fileName,$filePath){
   		try
   		{
			$data = file_get_contents(FCPATH.$filePath.$fileName); // Read the file's contents
			if($data != false){
				$name = $fileName;
				force_download($name, $data);
			}
		}
		catch(Exception $ex)
		{
			echo "Sorry .... File unavailable for download ! Please try after sometime.";
		}
    }
}