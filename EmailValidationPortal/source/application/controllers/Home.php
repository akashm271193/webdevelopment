<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
class Home extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->library('logging_lib',null,"logger");
		$this->load->library('session');
		$this->className = $this->router->class;
	
	}

	public function index() 
	{  
		//Check if error or link of end files is generated, and if yes , load he respective data 
		//and unset the session data, else just upload the upload page
		if($this->session->userdata('error') != "" && $this->session->userdata('error') != null ){
			$data['error'] = $this->session->userdata('error');
			$this->session->unset_userdata('error');
			$this->load->view('home',$data);
		}
		else if($this->session->userdata('fileLinks') != null){
			$data['fileLinks']  = $this->session->userdata('fileLinks');
			$data['transactionId']  = $this->session->userdata('transactionId');
			$this->session->unset_userdata('fileLinks');
			$this->session->unset_userdata('transactionId');
			$this->load->view('home',$data);
		}
		else {
			$this->session->unset_userdata('error');
			$this->session->unset_userdata('fileLinks');
			$this->load->view('home', $data);
		}
	}

	/** 
	  * Function to load the csv files
	  * and if successful, redirects to transaction controller to process
	  * the file.
	  *
	  * @params (NULL)
	  * @returns(NULL)
	  **/
	function upload(){
		//Get config parameters
		$uploadPath = $this->config->item('uploadPath');
		$maxFileSize = $this->config->item('uploadFileSize');
		
		//Get data from the form posted in the view
		$inputData['firstName']= $this->input->post('firstName');
		$inputData['lastName'] = $this->input->post('lastName');
		
		//Code to generate the file name & transaction ID.
		$newFileName = '';
		$newFileName .= substr($inputData['firstName'],0,1);
		$newFileName .= substr($inputData['lastName'],0,1);
		$newFileName .= time();
		
		$inputData['transactionId'] = $newFileName;

		//Code for uploading the csv file
		$config['upload_path'] = FCPATH.$uploadPath;
		$config['allowed_types'] = 'csv';
		$config['max_size']  = $maxFileSize;
		$config['file_name'] = $newFileName;
		
		$inputData['filePath'] = $uploadPath;
		
		$this->load->library('upload', $config);
		
		if (!$this->upload->do_upload('csvFile')){
			$this->session->set_userdata("error",$this->upload->display_errors());
			redirect('home');
		}
		else{
			$inputData['fileData'] = $this->upload->data();
			$this->session->set_userdata("inputFileData",$inputData);
			redirect('transaction/startTransaction');
		}
	}
	
	/** 
	  * Function to download the sample CSV file.
	  *
	  * @params (NULL)
	  * @returns(Download link for the sample csv file)
	  **/
	public function download(){
		//Get config parameters
		$sampleCsvFileName = $this->config->item('sampleCsvFileName');
		$downloadLink = $this->config->item("downloadLink");
		$fileLink = base_url().$downloadLink.$sampleCsvFileName;	
		echo $fileLink;
	}
	
	/** 
	  * Function to download the sample CSV file.
	  *
	  * @params (NULL)
	  * @returns(Download link for the sample csv file)
	  **/
	public function getTransactionHistory(){
		//Get config parameters
		$transactionId = $this->input->post('transactionId');
		$this->session->set_userdata("transactionId",$transactionId);
		redirect('transaction/getTransactionLinks');
		
	}
}