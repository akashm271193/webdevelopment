<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);

class Transaction extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('transaction_model',"transModel");	
		$this->load->library('logging_lib',null,"logger");
		$this->load->library('session');
		$this->className = $this->router->class;
	}

	public function index()
	{
			
	}

	/** 
	  * Function to start Transaction(processing of file)
	  * It creates an entry in the Transacction master table
	  * and if successful forwards the file to be processed.
	  *
	  * @params (Data stored in the session) 
	  **/
	function startTransaction(){
		$transactionData = $this->session->userdata('inputFileData');
		$this->session->unset_userdata('inputFileData');
		if($transactionData == null){
			$this->session->set_userdata("error","File data not present or session expired. Please try again !");
			redirect('home');
		}
		$transactionDbId = $this->transModel->insertIntoTransactionMaster($transactionData);
		if($transactionDbId != 0){
			$isProcessed = $this->processTransaction($transactionData,$transactionDbId);
			if($isProcessed === 0){
					$this->session->set_userdata("error","Error while processing the file! Please recheck the file again for issues else download the sample file and reupload.");
					redirect('home');
			}
		}
	}
	
	/** 
	  * Function to process the file, just uploaded by the previous function.
	  * Once processed, it calls the API to validate the data and updates the
	  * response recieved from Mail gun api. Once that is successful, it calls
	  * the generate files function.
	  *
	  * @params (transactionData  ---- Data read from the file,transactionDbId ---- Primary key for the entry in trans Master tbl)
	  * @returns (0 if there is any issue in processing data, or calling the api , or updating the response from the api.)
	  **/
	function processTransaction($transactionData,$transactionDbId)
	{
		$processedData = $this->transModel->processTransaction($transactionData,$transactionDbId);
		if($processedData != 0){
			$validatedData = $this->transModel->validateDataUsingApi($transactionDbId);
			if($validatedData != 0 ){
				$isSuccessful = $this->transModel->updateDataInTransactionData($validatedData,$transactionDbId);
				if($isSuccessful > 0){
					$this->generateFiles($transactionDbId,$transactionId);
				}
			}
		}
		return 0;	
	}	
	
	/** 
	  * Function to generate the files based on the response of the apis.
	  * We have 6 types of files -- Valid emails, corrected emails(Do you mean), disposable emails,
	  * role based emails, mailbox verification failed emails, and the entire data.
	  * Once the files are written, the call is forwarded to generate links to download the file.
	  *
	  * @params (transactionDbId ---- Primary key for the entry in trans Master tbl)
	  **/
	function generateFiles($transactionDbId)
	{
		$transactionId = $this->transModel->getTransactionIdFromTransactionMaster($transactionDbId);
		$data['validData'] = $this->transModel->getValidData($transactionDbId);
		$fileNames['validData'] = $this->transModel->generateFiles($data['validData'],0,$transactionId);
		$data['disposableData'] = $this->transModel->getDisposableData($transactionDbId);
		$fileNames['disposableData'] = $this->transModel->generateFiles($data['disposableData'],1,$transactionId);
		$data['correctedData']  = $this->transModel->getCorrectedData($transactionDbId);
		$fileNames['correctedData'] = $this->transModel->generateFiles($data['correctedData'],2,$transactionId);
		$data['roleBasedData']  = $this->transModel->getRoleBasedData($transactionDbId);
		$fileNames['roleBasedData'] = $this->transModel->generateFiles($data['roleBasedData'],3,$transactionId);
		$data['mailboxVerificationFailed'] = $this->transModel->getMailboxVerficiationFailedData($transactionDbId);
		$fileNames['mailboxVerificationFailed'] = $this->transModel->generateFiles($data['mailboxVerificationFailed'],4,$transactionId);
		$data['allData'] = $this->transModel->getAllDataByTransactionId($transactionDbId);
		$fileNames['allData'] = $this->transModel->generateFiles($data['allData'],5,$transactionId);
		$this->generateLinks($fileNames,$transactionDbId,$transactionId);
	}
	
	
	/** 
	  * Function to generate downloadable links for all the files.
	  * Once generated, we enter the links into the Transaction Response Table.
	  * And set error or links in the session and redirect bank to home.
	  *
	  * @params (fileNames  ---- Array of the files created,transactionDbId ---- Primary key for the entry in trans Master tbl)
	  **/
	function generateLinks($fileNames,$transactionDbId,$transactionId){
		
		$downloadLink = $this->config->item("downloadLink");
		foreach($fileNames as $type => $name){
			if($name === 0){
				$fileLinks[$type] = "File Not Generated";
			}
			else
				$fileLinks[$type] = base_url().$downloadLink.$name.".csv";			
		}
		$isSuccessful = $this->transModel->insertIntoTransactionResponseFile($fileLinks,$transactionDbId);
		if($isSuccessful == 0){
			$this->session->set_userdata("error","Error while generating files. Please try again !!");
		}
		else{
			$this->session->set_userdata("fileLinks",$fileLinks);
			$this->session->set_userdata("transactionId",$transactionId);
		}
		redirect('home');
	}
	
	/** 
	  * Function to retrieve downloadable links for all the filesby using Transaction ID.
	  * If all is proper we set links else we set error in the session and redirect bank to home.
	  *
	  * @params (null)
	  * @return (null)
	  **/
	function getTransactionLinks(){
		
		$transactionId = $this->session->userdata('transactionId');
		$this->session->unset_userdata('transactionId');
		
		if($transactionId == null || $transactionId == ""){
			$this->session->set_userdata("error","Invalid Transaction ID !! Please try again.");
		}
		$links = $this->transModel->getResponseFilesByTransactionId($transactionId);
		foreach($links as $row){
			$fileLinks[$row['csvFileType']] = $row['csvFilePath'];	
		}
		if(sizeOf($fileLinks)>0){
			$this->session->set_userdata("fileLinks",$fileLinks);
			$this->session->set_userdata("transactionId",$transactionId);
		}
		else{
			$this->session->set_userdata("error","No records found for the transaction !! Please try again.");
		}
		redirect('home');
	}
}
