<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Transaction_model extends CI_Model
{
    public function __construct(){
		
    	$this->load->database('default');
		$this->load->library('CsvReaderWriter',null,"csvlib");
		$this->load->library('Mailgun_api_lib',null,"mailApi");
		$this->load->library('logging_lib',null,"logger");
		
		// Global variables for table names
		$this->tblTransactionMaster = "TransactionMaster";
		$this->tblTransactionData = "TransactionData";
		$this->tblTransactionResponseFile = "TransactionResponseFile";
    }
	
    /** 
	  * Adds a unique entry of every file upload (transaction) into Transaction Master
	  * 
	  * @params (Data array having keys firstName,LastName, Transaction id, and uploaded file metadata as fileData)
	  * @returns(Primary key (id) if success, 0 if failed)
	  *	  
	  **/
	public function insertIntoTransactionMaster($transactionData)
	{
		$data['firstName'] = $transactionData['firstName'];
		$data['lastName'] = $transactionData['lastName'];
		$data['transactionId'] = $transactionData['transactionId'];
		$data['csvFileName'] = $transactionData['fileData']['file_name'];
		$data['createdTimestamp'] = date("Y-m-d H:i:s",time());
		$result = $this->db->insert($this->tblTransactionMaster,$data);
		if($result == 1){
			$insert_id = $this->db->insert_id();
			return $insert_id;
		}
		
		return 0 ;
	}

    /** 
	  * Function to control the flow of getting data from the uploaded file and pass it for insertion in transactionData table.
	  * 
	  * @params (transactionData ------- Data array having keys firstName,LastName, Transaction id, and uploaded file metadata as fileData
	  *			 transactionDbId ------- Primary key (id) of transactionMaster table)
	  * @returns(processedData of successful, 0 if failed)
	  *	  
	  **/	  
	public function processTransaction($transactionData,$transactionDbId){
		$filePath = $transactionData['filePath'].$transactionData['fileData']['file_name'];
		$fileData = $this->getDataFromFile($filePath,$transactionData['fileData']['file_name']);
		if($fileData == 0)
			return 0;
		$processedData = $this->insertIntoTransactionData($fileData,$transactionDbId,$transactionData['fileData']['file_name']);
		return $processedData;
	}  
	
	
	/** 
	  * Function to get data from the uploaded file.
	  * 
	  * @params (filePath ------- Path to the location where file is uploaded)
	  * @returns(Whatever is returned by the parse_file function in csvReaderWriter Lib)
	  *	  
	  **/
	public function getDataFromFile($filePath,$fileName){
		$fileData = $this->csvlib->parse_file($filePath);
		$isValid = $this->validateFileData($fileData['columns'],$fileName);
		if($isValid == 1)
			return $fileData['content'];
		else
			return 0;
	}
	
	/** 
	  * Function to validate if the columns passed are correct or not.
	  * 
	  * @params (fileDataColumns ------- Array of columns present in the file)
	  * @returns(true if valid and false if invalid)
	  *	  
	  **/
	public function validateFileData($fileDataColumns,$fileName){
		$csvEmailCol     = $this->config->item("csvEmailCol"); 
		$csvFirstNameCol = $this->config->item("csvFirstNameCol");
		$csvLastNameCol  = $this->config->item("csvLastNameCol");
		$csvZipCodeCol   = $this->config->item("csvZipCodeCol");
		$columnArr = array($csvEmailCol,$csvFirstNameCol,$csvLastNameCol,$csvZipCodeCol);
		$fileDataColumns = array_map('trim', $fileDataColumns);
		$validCounter = 0 ;
		foreach($fileDataColumns as $column){
			if(in_array($column,$columnArr)){
				$validCounter++;
			}
		}			
		if($validCounter == sizeOf($fileDataColumns)){
			$this->logger->logInfo("File successfully validated : ".$fileName.". All ".$validCounter." columns are valid.","fileValidation");
			return 1;
		}
		else{
			$this->logger->logError("Error in validating file : ".$fileName.". Only ".$validCounter." of ".sizeOf($fileDataColumns)." columns are valid.","fileValidation");
			return 0;
		}
	}
	
	
    /** 
	  * Adds data read from file(transaction) into Transaction Data table.
	  * 
	  * @params (fileData ----- Data array having data read from file, transactionDbId ------- Primary key (id) of transactionMaster table)
	  * @returns(Rows inserted if successful, 0 if failed)
	  *	  
	  **/
	public function insertIntoTransactionData($fileData,$transactionDbId,$fileName)
	{
		$csvEmailCol     = $this->config->item("csvEmailCol"); 
		$csvFirstNameCol = $this->config->item("csvFirstNameCol");
		$csvLastNameCol  = $this->config->item("csvLastNameCol");
		$csvZipCodeCol   = $this->config->item("csvZipCodeCol");
		$transactionData = array();
		$fileDataLength = sizeof($fileData);
		for ($index = 0 ; $index < $fileDataLength; $index++){
			$transactionData[$index]['email'] = $fileData[$index][$csvEmailCol];
			$transactionData[$index]['firstName'] = $fileData[$index][$csvFirstNameCol];
			$transactionData[$index]['lastName'] = $fileData[$index][$csvLastNameCol];
			$transactionData[$index]['zipCode'] = $fileData[$index][$csvZipCodeCol];
			$transactionData[$index]['transactionId'] = $transactionDbId;
			$transactionData[$index]['createdTimestamp'] = date("Y-m-d H:i:s",time());
		}
		
		$this->db->trans_start();
		$this->db->insert_batch($this->tblTransactionData,$transactionData);
		$this->db->trans_complete();
		
		$this->db->where('transactionId',$transactionDbId);
		$insertCount = $this->db->count_all_results($this->tblTransactionData);
		
		if($insertCount == $fileDataLength){
			$this->logger->logInfo("Data successfully inserted for file : ".$fileName.". All ".$insertCount." rows inserted.","fileValidation");
			return $transactionData;
		}
		else{
			$this->logger->logError("Error in inserting data for file : ".$fileName.". Only ".$insertCount." of ".$fileDataLength." rows inserted.","fileValidation");
			return 0;
		}
	}
	
	/** 
	  * Function to get the data from transactionData table and call the Mail gun api library.
	  * 
	  * @params (transactionDbId ------- Primary key (id) of transactionMaster table)
	  * @returns(Whatever is returned by the validate Emails function in Mail gun Api lib)
	  *	  
	  **/
	public function validateDataUsingApi($transactionDbId)
	{
		$processedData = $this->getEmailsByTransactionDbId($transactionDbId);
		$validatedData = $this->mailApi->validateEmails($processedData);
		return $validatedData;
	}

	/** 
	  * Function to get the id and email from transactionData table using id (Transaction Master pk).
	  * 
	  * @params (transactionDbId ------- Primary key (id) of transactionMaster table)
	  * @returns(Array of arrays of id and email)
	  *
	  **/	
	public function getEmailsByTransactionDbId($transactionDbId)
	{
		$this->db->select('id,email');
		$this->db->order_by('id','asc');
		$result = $this->db->get_where($this->tblTransactionData,array('transactionId'=>$transactionDbId));
		return $result->result_array();
	}
	
	/** 
	  * Function to get the id and email from transactionData table using id (Transaction Master pk).
	  * 
	  * @params (transactionDbId ------- Primary key (id) of transactionMaster table)
	  * @returns(Array of arrays of id and email)
	  *
	  **/	
	public function updateDataInTransactionData($validatedData,$transactionDbId)
	{
		$this->db->trans_start();
		$rowsUpdated = $this->db->update_batch($this->tblTransactionData, $validatedData,'id');
		$this->db->trans_complete();
		return $rowsUpdated;
	}
	
	/** 
	  * Function to get all the rows from transactionData table using id (Transaction Master pk) where isValid is true.
	  * 
	  * @params (transactionDbId ------- Primary key (id) of transactionMaster table)
	  * @returns(Array of rows of valid data)
	  *
	  **/
	public function getValidData($transactionDbId)
	{
		$this->db->select('*');
		$this->db->order_by('id','asc');
		$result = $this->db->get_where($this->tblTransactionData,array('transactionId'=>$transactionDbId,"isValid"=>1));
		return $result->result_array();
	}
	
	/** 
	  * Function to get all the rows from transactionData table using id (Transaction Master pk) where didYouMean is not blank.
	  * 
	  * @params (transactionDbId ------- Primary key (id) of transactionMaster table)
	  * @returns(Array of rows of corrected data)
	  *
	  **/
	public function getCorrectedData($transactionDbId)
	{
		$this->db->select('*');
		$this->db->order_by('id','asc');
		$result = $this->db->get_where($this->tblTransactionData,array('transactionId'=>$transactionDbId,"didYouMean"=>" != ''"));
		return $result->result_array();
	}
	
	/** 
	  * Function to get all the rows from transactionData table using id (Transaction Master pk) where isDisposable is true.
	  * 
	  * @params (transactionDbId ------- Primary key (id) of transactionMaster table)
	  * @returns(Array of rows of disposable emails)
	  *
	  **/
	public function getDisposableData($transactionDbId)
	{
		$this->db->select('*');
		$this->db->order_by('id','asc');
		$result = $this->db->get_where($this->tblTransactionData,array('transactionId'=>$transactionDbId,"isDisposable"=>1));
		return $result->result_array();
	}

	/** 
	  * Function to get all the rows from transactionData table using id (Transaction Master pk) where isRoleAddress is true.
	  * 
	  * @params (transactionDbId ------- Primary key (id) of transactionMaster table)
	  * @returns(Array of rows of role based data)
	  *
	  **/
	public function getRoleBasedData($transactionDbId)
	{
		$this->db->select('*');
		$this->db->order_by('id','asc');
		$result = $this->db->get_where($this->tblTransactionData,array('transactionId'=>$transactionDbId,"isRoleAddress"=>1));
		return $result->result_array();
	}
	
	/** 
	  * Function to get all the rows from transactionData table using id (Transaction Master pk) where mailboxVerification is true.
	  * 
	  * @params (transactionDbId ------- Primary key (id) of transactionMaster table)
	  * @returns(Array of rows of mailbox verification failed data)
	  *
	  **/
	public function getMailboxVerficiationFailedData($transactionDbId)
	{
		$this->db->select('*');
		$this->db->order_by('id','asc');
		$result = $this->db->get_where($this->tblTransactionData,array('transactionId'=>$transactionDbId,"mailboxVerification"=>0));
		return $result->result_array();
	}
	
	/** 
	  * Function to get all the rows from transactionData table using id (Transaction Master pk).
	  * 
	  * @params (transactionDbId ------- Primary key (id) of transactionMaster table)
	  * @returns(Array of rows of valid data)
	  *
	  **/
	public function getAllDataByTransactionId($transactionDbId)
	{
		$this->db->select('*');
		$this->db->order_by('id','asc');
		$result = $this->db->get_where($this->tblTransactionData,array('transactionId'=>$transactionDbId));
		return $result->result_array();
	}
	
	/** 
	  * Function to get Transaction id using id from Transaction Master .
	  * 
	  * @params (transactionDbId ------- Primary key (id) of transactionMaster table)
	  * @returns(Transaction ID of the transaction)
	  *
	  **/
	public function getTransactionIdFromTransactionMaster($transactionDbId)
	{
		$this->db->select('transactionId');
		$this->db->order_by('id','asc');
		$result = $this->db->get_where($this->tblTransactionMaster,array('id'=>$transactionDbId));
		return $result->row()->transactionId; 
	}

	/** 
	  * Function to get generate File for the required type of data.
	  * Types :
	  * 0 - Valid , 1 - Disposable , 2 - Corrected (Did you mean), 3 - Role based data , 4 - Mailbox Verfication failed, 5 - Entire data
	  *
	  * @params ($data ---- Array of arrays containing all types of data,$type --- Int for mentioning which type of data it is,
	  *			transactionId ------- transactionId from transactionMaster table)
	  * @returns(The name of the file generated if successful, 0 if failed)
	  *
	  **/
	public function generateFiles($data,$type,$transactionId){
		$fileData = array();
		$fileData['headers'] = array('First Name','Last Name','Zip Code','Email','Is Valid');
		$fileName = $transactionId."_";
		switch($type){
			case 0:
				array_push($fileData['headers'],'Created Time','Updated Time');
				for($counter = 0 ;$counter < sizeOf($data);$counter++){
					$fileData['data'][$counter][0] = $data[$counter]['firstName'];
					$fileData['data'][$counter][1] = $data[$counter]['lastName'];
					$fileData['data'][$counter][2] = $data[$counter]['zipCode'];
					$fileData['data'][$counter][3] = $data[$counter]['responseAddress'];
					$fileData['data'][$counter][4] = ($data[$counter]['isValid'] == 0) ? "false" : "true";
					$fileData['data'][$counter][5] = $data[$counter]['createdTimestamp'];
					$fileData['data'][$counter][6] = $data[$counter]['updatedTimestamp'];
				}
				$fileName .= "IsValid";
			break;
			
			case 1:
				array_push($fileData['headers'],'Is Disposble','Created Time','Updated Time');
				for($counter = 0 ;$counter < sizeOf($data);$counter++){
					$fileData['data'][$counter][0] = $data[$counter]['firstName'];
					$fileData['data'][$counter][1] = $data[$counter]['lastName'];
					$fileData['data'][$counter][2] = $data[$counter]['zipCode'];
					$fileData['data'][$counter][3] = $data[$counter]['responseAddress'];
					$fileData['data'][$counter][4] = ($data[$counter]['isValid'] == 0) ? "false" : "true";
					$fileData['data'][$counter][5] = ($data[$counter]['isDisposable']  == 0) ? "false" : "true" ;
					$fileData['data'][$counter][6] = $data[$counter]['createdTimestamp'];
					$fileData['data'][$counter][7] = $data[$counter]['updatedTimestamp'];
				}
				$fileName .= "IsDisp";
			break;
			
			case 2:
				array_push($fileData['headers'],'Did You Mean','Created Time','Updated Time');
				for($counter = 0 ;$counter < sizeOf($data);$counter++){
					$fileData['data'][$counter][0] = $data[$counter]['firstName'];
					$fileData['data'][$counter][1] = $data[$counter]['lastName'];
					$fileData['data'][$counter][2] = $data[$counter]['zipCode'];
					$fileData['data'][$counter][3] = $data[$counter]['responseAddress'];
					$fileData['data'][$counter][4] = ($data[$counter]['isValid'] == 0) ? "false" : "true";
					$fileData['data'][$counter][5] = $data[$counter]['didYouMean'];
					$fileData['data'][$counter][6] = $data[$counter]['createdTimestamp'];
					$fileData['data'][$counter][7] = $data[$counter]['updatedTimestamp'];
				}
				$fileName .= "IsCorr";
			break;
			
			case 3:
				array_push($fileData['headers'],'Is Role Based','Created Time','Updated Time');
				for($counter = 0 ;$counter < sizeOf($data);$counter++){
					$fileData['data'][$counter][0] = $data[$counter]['firstName'];
					$fileData['data'][$counter][1] = $data[$counter]['lastName'];
					$fileData['data'][$counter][2] = $data[$counter]['zipCode'];
					$fileData['data'][$counter][3] = $data[$counter]['responseAddress'];
					$fileData['data'][$counter][4] = ($data[$counter]['isValid'] == 0) ? "false" : "true";
					$fileData['data'][$counter][5] = ($data[$counter]['isRoleAddress'] == 0) ? "false" : "true";
					$fileData['data'][$counter][6] = $data[$counter]['createdTimestamp'];
					$fileData['data'][$counter][7] = $data[$counter]['updatedTimestamp'];
				}
				$fileName .= "IsRole";
			break;
			
			case 4:
				array_push($fileData['headers'],'Mailbox Verfication','Created Time','Updated Time');
				for($counter = 0 ;$counter < sizeOf($data);$counter++){
					$fileData['data'][$counter][0] = $data[$counter]['firstName'];
					$fileData['data'][$counter][1] = $data[$counter]['lastName'];
					$fileData['data'][$counter][2] = $data[$counter]['zipCode'];
					$fileData['data'][$counter][3] = $data[$counter]['responseAddress'];
					$fileData['data'][$counter][4] = ($data[$counter]['isValid']  == 0) ? "false" : "true";
					$fileData['data'][$counter][5] = ($data[$counter]['mailboxVerification'] == 0) ? "false" : "true";
					$fileData['data'][$counter][6] = $data[$counter]['createdTimestamp'];
					$fileData['data'][$counter][7] = $data[$counter]['updatedTimestamp'];
				}
				$fileName .= "MbVeri";
			break;
			
			case 5:
				array_push($fileData['headers'],'Did You Mean','Is Disposable','Is Role Address','Mailbox Verfication','Parts','Reason','Created Time','Updated Time');
				for($counter = 0 ;$counter < sizeOf($data);$counter++){
					$fileData['data'][$counter][0]  = $data[$counter]['firstName'];
					$fileData['data'][$counter][1]  = $data[$counter]['lastName'];
					$fileData['data'][$counter][2]  = $data[$counter]['zipCode'];
					$fileData['data'][$counter][3]  = $data[$counter]['responseAddress'];
					$fileData['data'][$counter][4]  = ($data[$counter]['isValid']  == 0) ? "false" : "true";
					$fileData['data'][$counter][5]  = $data[$counter]['didYouMean'];
					$fileData['data'][$counter][6]  = ($data[$counter]['isDisposable'] == 0) ? "false" : "true";
					$fileData['data'][$counter][7]  = ($data[$counter]['isRoleAddress'] == 0) ? "false" : "true";
					$fileData['data'][$counter][8]  = ($data[$counter]['mailboxVerification'] == 0) ? "false" : "true";
					$fileData['data'][$counter][9]  = $data[$counter]['parts'];
					$fileData['data'][$counter][10] = $data[$counter]['reason'];
					$fileData['data'][$counter][11] = $data[$counter]['createdTimestamp'];
					$fileData['data'][$counter][12] = $data[$counter]['updatedTimestamp'];
				}
				$fileName .= "All";
			break;
		}
		$isSuccessful = $this->csvlib->write_data($fileName,$fileData);
		if($isSuccessful){
			$this->logger->logInfo("File successfully generated : ".$fileName.".csv","fileGeneration");
			return $fileName;
		}
		else{
			$this->logger->logError("Error in creating file : ".$fileName.".csv","fileGeneration");
			return 0;
		}
	}
	
	
	/** 
	  * Function to insert the links of response file generated into transaction Response Table.
	  * 
	  * @params (fileLinks ---- array containing different links of types of file generated,transactionDbId ------- Primary key (id) of transactionMaster table)
	  * @returns(Insert count if successful , 0 if failed)
	  *
	  **/
    public function insertIntoTransactionResponseFile($fileLinks,$transactionDbId){
		$responseFiledata = array();
		$counter = 0;
		foreach($fileLinks as $type => $link){
				$responseFiledata[$counter]['transactionId'] = $transactionDbId;
				$responseFiledata[$counter]['csvFileType'] = $type;
				$responseFiledata[$counter]['csvFilePath'] = $link;
				$counter++;
		}
		
		$this->db->trans_start();
		$this->db->insert_batch($this->tblTransactionResponseFile,$responseFiledata);
		$this->db->trans_complete();
		
		$this->db->where('transactionId',$transactionDbId);
		$insertCount = $this->db->count_all_results($this->tblTransactionResponseFile);
		
		if($insertCount == $counter)
			return $insertCount;
		else
			return 0;
		
	}
	
	/** 
	  * Function to get all links generated and stored in Transaction Response File by using transaction Id i Transaction Master.
	  * 
	  * @params (transactionId ------- transactionId column of transactionMaster table)
	  * @returns(Rows found as an array)
	  *
	  **/
    public function getResponseFilesByTransactionId($transactionId){
		
		$this->db->select('tr.csvFileType,tr.csvFilePath');
		$this->db->join($this->tblTransactionMaster.' as tm',"tm.id = tr.transactionId");
		$result = $this->db->get_where($this->tblTransactionResponseFile.' as tr',array("tm.transactionId"=>$transactionId));
		return $result->result_array();
	}
}