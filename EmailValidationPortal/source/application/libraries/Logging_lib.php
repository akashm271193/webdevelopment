<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
	Class to send push notifications using Google Cloud Messaging for Android
	Example usage
	-----------------------
	$an = new GCMPushMessage($apiKey);
	$an->setDevices($devices);
	$response = $an->send($message);
	-----------------------
	
	$apiKey Your GCM api key
	$devices An array or string of registered device tokens
	$message The mesasge you want to push out
	@author Matt Grundy
	Adapted from the code available at:
	http://stackoverflow.com/questions/11242743/gcm-with-php-google-cloud-messaging
*/
class Logging_lib {
	
	public function __construct()
	{
		$this->ci =& get_instance();
		$this->logPath = $this->ci->config->item('log_path');
	}

	
	/*
		Send the message to the device
		@param $message The message to send
		@param $data Array of data to accompany the message
	*/
	public function logError($msg,$className){
		$location  = FCPATH.$this->logPath.$className.'/'.date("Y-m-d").'_error.json';
 	  	$myfile = fopen($location, "a");
      	fwrite($myfile, date('Y-m-d H:i:s')." LOG : [ERROR] - ".$msg.PHP_EOL);
      	fclose($myfile);
      	return true;
	}

	/*
		Send the message to the device
		@param $message The message to send
		@param $data Array of data to accompany the message
	*/
	public function logInfo($msg,$className){
		$location  = FCPATH.$this->logPath.$className.'/'.date("Y-m-d").'_info.json';
 	  	$myfile = fopen($location, "a");
      	fwrite($myfile, date('Y-m-d H:i:s')." LOG : [INFO] - ".$msg.PHP_EOL);
      	fclose($myfile);
      	return true ;
	}

	/*
		Send the message to the device
		@param $message The message to send
		@param $data Array of data to accompany the message
	*/
	public function logDbQueries($msg,$className){
		$location  = FCPATH.$this->logPath.$className.'/'.date("Y-m-d").'_dbqueries.json';
 	  	$myfile = fopen($location, "a");
      	fwrite($myfile, date('Y-m-d H:i:s')." LOG : [INFO] - ".$msg);
      	fclose($myfile);
      	return true ;
	}

	/*
		Send the message to the device
		@param $message The message to send
		@param $data Array of data to accompany the message
	*/
	public function logDbError($msg,$className){
		$location  = FCPATH.$this->logPath.$className.'/'.date("Y-m-d").'_dbqueries.json';
 	  	$myfile = fopen($location, "a");
      	fwrite($myfile, date('Y-m-d H:i:s')." LOG : [ERROR] - ".$msg);
      	fclose($myfile);
      	return true ;
	}
	
	/*
		Send the message to the device
		@param $message The message to send
		@param $data Array of data to accompany the message
	*/
	public function logData($msg,$className){
		$location  = FCPATH.$this->logPath.$className.'/'.date("Y-m-d").'_data.json';
 	  	$myfile = fopen($location, "a");
      	fwrite($myfile, date('Y-m-d H:i:s')." LOG : [INFO] - ".$msg);
      	fclose($myfile);
      	return true ;
	}
}
