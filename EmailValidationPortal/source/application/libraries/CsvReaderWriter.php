<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class CsvReaderWriter {

var $fields;        /** columns names retrieved after parsing */
var $separator = ',';    /** separator used to explode each line */
	
	
	public function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->library('logging_lib',null,"logger");
	}

	/** 
	  * Parse through the text read from the file
	  * 
	  * @params ($inputText Raw data of the file)
	  * @returns(Whatever is returned by function parse_lines)
	  **/
	function parse_text($inputText) {
		$lines = explode("\n", $inputText);
		return $this->parse_lines($lines);
	}

	/** 
	  * Read the file passed and parse it.
	  * 
	  * @params ($inputFilePath Path of the file to be read)
	  * @returns(Whatever is returned by function parse_lines)
	  **/
	function parse_file($inputFilepath) {
		$lines = file($inputFilepath);
		return $this->parse_lines($lines);
	}


	/** 
	  * Parse the lines and create an array of rows containing data as column->value format.
	  * 
	  * @params ($inputCsvLines Data to be parsed)
	  * @returns(Array of arrays(rows) containing indexed value (E.g. column => value))
	  **/
	function parse_lines($inputCsvLines) {
		$content = FALSE;
		foreach( $inputCsvLines as $lineNum => $line ) {
			if( $line != '' ) { // skip empty lines
				$elements = split($this->separator, $line);

				if(!is_array($content) ) { // the first line contains fields names
					$this->fields = $elements;
					$content = array();
				} else {
					$item = array();
					foreach( $this->fields as $id => $field) {
						if( isset($elements[$id]) ) {
							$item[trim($field)] = trim($elements[$id]);
						}
					}
					$content[] = $item;
				}
			}
		}
		$fileDataWithCol['columns'] = $this->fields;
		$fileDataWithCol['content'] = $content;
		return $fileDataWithCol;
	}

	/** 
	  * Write the data passed into a new csv file
	  * 
	  * @params ($fileName ----- Name of the new file , $fileData ----- Data to be written in the file, containing two arrays - headers & data).
	  * @returns(true if successful , false if failed)
	  **/
	function write_data($fileName,$fileData){
		try { 
			//$filePath = "./endfiles/";
			$filePath = $this->ci->config->item('endFilesPath');
			$file = fopen(FCPATH.$filePath.$fileName.'.csv', 'w');
		 
			// save the column headers
			fputcsv($file, $fileData['headers']);
		 
		 
			// save each row of the data
			foreach ($fileData['data'] as $row)
			{
				fputcsv($file, $row);
			}
		 
			// Close the file
			fclose($file);	
		} catch(Exception $ex){
			$this->logger->logError("Error while writing into file : ".$fileName.".csv  ======  Error : ".$ex->getMessage(),"fileGeneration");
			return false;
		}
		return true;
	}
}