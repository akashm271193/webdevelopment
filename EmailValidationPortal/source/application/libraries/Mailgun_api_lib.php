<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
	Class to call MailGun Api for validating emails
	Example usage
	-----------------------
	$mailgun = new Mailgun($apiKey);
	$response = $mailgun->get("address/validate", array('address' => $emailId));
	-----------------------
	
	$apiKey Your credentials provided by MailGun
	$emailId A string of single email Id
*/

require (__DIR__.'/../../vendor/autoload.php');
use Mailgun\Mailgun;

class Mailgun_api_lib {
	
	public function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->library('logging_lib',null,"logger");
		$apiKey = $this->ci->config->item('mailGunApiKey');
		$this->mailgun = new Mailgun($apiKey);
	}

	
	/** 
	  * Parse through a list of emails and call individual function validateEmail to validate Email
	  * take the response for each of them and forward it to processValidateEmailApiResponse function.
	  * 
	  * @params ($dataList Array of emails to be validated)
	  * @returns(validatedEmails --- array of processed response, 0 if failed)
	  *	  
	  **/
	public function validateEmails($dataList){
		$validatedEmails = array();
		foreach($dataList as $rowData){
			try {
				# Issue the call to the client.
				$response = $this->validateEmail($rowData['email']);
				$message = "Request : ".json_encode($rowData)." ====== Response : ".json_encode($response);
				$this->ci->logger->logInfo($message,"apiResponse");
				if($response != 0 )
					$validatedEmails = $this->processValidateEmailApiResponse($response,$rowData,$validatedEmails);
				else 
					return 0;
			} catch (Exception $e) {
				return 0;
			}
		}
		return $validatedEmails;
	}
	
	/** 
	  * Function to call the Mailgun Api	  
	  * 
	  * @params ($emailId ------ email to be validated)
	  * @returns(Response from the api if successful, 0 if failed)
	  *	  
	  **/
	public function validateEmail($emailId){
			try {
				# Issue the call to the client.
				$response = $this->mailgun->get("address/validate", array('address' => $emailId));
				return $response;
			} catch (Exception $e) {
				$this->ci->logger->logError("Error while calling MailGun Lib : ".json_encode($e->getMessage()),"apiResponse");
				return 0;
			}
	}
	
	/** 
	  * Parse through a response of the mailgunApi.
	  * 
	  * @params ($response ---- Response from Mail Gun APi , $data ---- Array containing id & email,$validatedEmails ---- Object in which result is to be stored (Output))
	  * @returns(validatedEmails ---- updated version of validated mails containing the newly parsed response)
	  *	  
	  **/
	public function processValidateEmailApiResponse($response,$data,$validatedEmails){
		$responseArr['responseCode'] = $response->http_response_code;
		$responseArr['responseAddress'] = $response->http_response_body->address;
		$responseArr['didYouMean'] = $response->http_response_body->did_you_mean;
		$responseArr['isDisposable'] = $response->http_response_body->is_disposable_address;
		$responseArr['isRoleAddress'] = $response->http_response_body->is_role_address;
		$responseArr['isValid'] = $response->http_response_body->is_valid;
		$responseArr['mailboxVerification'] = $response->http_response_body->mailbox_verification;
		$responseArr['parts'] = json_encode((array)$response->http_response_body->parts);
		$responseArr['reason'] = $response->http_response_body->reason;
		$responseArr['id'] = $data['id'];
		array_push($validatedEmails,$responseArr);
		return $validatedEmails;
	}
}
