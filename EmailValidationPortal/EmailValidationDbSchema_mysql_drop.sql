ALTER TABLE `ApiResponseFile` DROP FOREIGN KEY `ApiResponseFile_fk0`;

ALTER TABLE `TransactionCompleteLog` DROP FOREIGN KEY `TransactionCompleteLog_fk0`;

ALTER TABLE `TransactionCompleteLog` DROP FOREIGN KEY `TransactionCompleteLog_fk1`;

ALTER TABLE `TransactionResponseFile` DROP FOREIGN KEY `TransactionResponseFile_fk0`;

ALTER TABLE `TransactionData` DROP FOREIGN KEY `TransactionData_fk0`;

DROP TABLE IF EXISTS `TransactionMaster`;

DROP TABLE IF EXISTS `ApiResponseFile`;

DROP TABLE IF EXISTS `TransactionCompleteLog`;

DROP TABLE IF EXISTS `TransactionResponseFile`;

DROP TABLE IF EXISTS `TransactionData`;

