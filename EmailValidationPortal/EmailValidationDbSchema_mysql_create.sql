CREATE TABLE `TransactionMaster` (
	`id` int NOT NULL AUTO_INCREMENT,
	`transactionId` varchar(50) NOT NULL,
	`firstName` varchar(50) NOT NULL DEFAULT '"NoName"',
	`lastName` varchar(50) NOT NULL DEFAULT '"NoName"',
	`csvFileName` varchar(50) NOT NULL,
	`createdTimestamp` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	`updatedTimestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
);

CREATE TABLE `ApiResponseFile` (
	`id` int NOT NULL AUTO_INCREMENT,
	`transactionId` int NOT NULL,
	`responseFileName` varchar(100) NOT NULL,
	`createdTimestamp` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	`updatedTimestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
);

CREATE TABLE `TransactionCompleteLog` (
	`id` int NOT NULL AUTO_INCREMENT,
	`transactionId` int NOT NULL,
	`apiResponseId` int NOT NULL,
	`endFileCount` int NOT NULL DEFAULT '0',
	`createdTimestamp` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	`updatedTimestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
);

CREATE TABLE `TransactionResponseFile` (
	`id` int NOT NULL AUTO_INCREMENT,
	`transactionId` int NOT NULL,
	`csvFileType` varchar(50) NOT NULL,
	`csvFilePath` varchar(200) NOT NULL,
	`createdTimestamp` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	`updatedTimestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
);

CREATE TABLE `TransactionData` (
	`id` int NOT NULL AUTO_INCREMENT,
	`transactionId` int NOT NULL,
	`email` varchar(350) NOT NULL,
	`firstName` varchar(50) DEFAULT 'NULL',
	`lastName` varchar(50) DEFAULT 'NULL',
	`zipCode` varchar(10) DEFAULT 'NULL',
	`responseCode` INT(5) NOT NULL,
	`responseAddress` varchar(350) NOT NULL,
	`didYouMean` varchar(350) NOT NULL,
	`isDisposable` tinyint(1) NOT NULL DEFAULT '0',
	`isRoleAddress` tinyint(1) NOT NULL DEFAULT '0',
	`isValid` tinyint(1) NOT NULL DEFAULT '0',
	`mailboxVerification` tinyint(1) NOT NULL DEFAULT '0',
	`parts` varchar(500) NOT NULL,
	`reason` varchar(1000) NOT NULL,
	`createdTimestamp` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	`updatedTimestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
);

ALTER TABLE `ApiResponseFile` ADD CONSTRAINT `ApiResponseFile_fk0` FOREIGN KEY (`transactionId`) REFERENCES `TransactionMaster`(`id`);

ALTER TABLE `TransactionCompleteLog` ADD CONSTRAINT `TransactionCompleteLog_fk0` FOREIGN KEY (`transactionId`) REFERENCES `TransactionMaster`(`id`);

ALTER TABLE `TransactionCompleteLog` ADD CONSTRAINT `TransactionCompleteLog_fk1` FOREIGN KEY (`apiResponseId`) REFERENCES `ApiResponseFile`(`id`);

ALTER TABLE `TransactionResponseFile` ADD CONSTRAINT `TransactionResponseFile_fk0` FOREIGN KEY (`transactionId`) REFERENCES `TransactionMaster`(`id`);

ALTER TABLE `TransactionData` ADD CONSTRAINT `TransactionData_fk0` FOREIGN KEY (`transactionId`) REFERENCES `TransactionMaster`(`id`);

